# Script
# 1. Clonar el Repositorio
# 2. Seleccionar Tag de Iteracion
# 3. Seleccionar el entorno (Desarrollo, Produccion)
# 3. Desplegar
# 4. Poblar la BD con Datos de Prueba

echo "Deseas clonar el proyecto? y/N"
read resp

if [ resp = "y" || resp = "Y" ]; then
    git clone
    #Clonamos el proyecto
    echo "Clonando el proyecto..."
    git clone https://gitlab.com/ramirez-sebas1010/scrumpy-front-end.git
    echo "Clonado completado"
else
    echo "No se ha clonado el proyecto"
fi

#Verificamos que envie por lo menos el tag que queremo cambiar
if [ $# -eq 1 ] 
    then
    echo "Los Tags son:"
    git tag -l | grep -w $1

    if [ ! $? -eq 0 ] 
        then
        echo "Tag $1 No Existe, Finalizando el programa"
        exit 1
    fi


    if [ $# -eq 1 ]; then
        echo "Iremos al tag '$1' en 'Master'"
        git checkout $1
    fi

else
    echo "Usage: $0 <tag>"
    echo "Example: $0 iteracion1"
    echo "Example: $0 iteracion_3"
    exit 1
fi

