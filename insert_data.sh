#Antes que nada tiene que estar corriendo el contenedor
#Crear el superuser con
# check if arguments is greater than 0 and less than 3
if [ $# -gt 0 ] && [ $# -lt 4 ]; then
    docker exec -it scrumpy-back-end_web_1 python src/manage.py createsuperuser --email $1 --first_name $2 --last_name $3
    docker exec -it scrumpy-back-end_db_1 psql -U postgres -d postgres -f home/load_data.sql
else
    docker exec -it scrumpy-back-end_web_1 python src/manage.py createsuperuser
    docker exec -it scrumpy-back-end_db_1 psql -U postgres -d postgres -f home/load_data.sql  
fi
