from django.contrib import admin
from .models import Project, ProjectPermission, Member
# Register your models here.

admin.site.register(Project)
admin.site.register(ProjectPermission)
admin.site.register(Member)