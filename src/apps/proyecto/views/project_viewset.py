from proyecto.models import Project, Member, ProjectRole, ProjectPermission
from proyecto.serializers import ListOrRetrieveProjectSerializer, CreateProjectSerializer
from rest_framework.response import Response
from rest_framework import views, viewsets, status

"""
 * @apiGroup Proyecto
 * @api {post} /projectModule/projects/ Crear un Projecto
 * @apiDescription Crear un proyecto
 * @apiHeader {String} Autorization JWT TOKEN
 * @apiBody {String} name Nombre del proyecto
 * @apiBody {String} description Descripcion del proyecto
 * @apiBody {String} scrum_master Email del usuario que será el scrum master
 * @apiBody {String} status Estado del proyecto
 * @apiBody {String} startDay Fecha de inicio del proyecto formato YYYY-MM-DD
 * @apiBody {String} endDay Fecha de fin del proyecto formato YYYY-MM-DD
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "": "",
 *       "": ""
 *     }
"""

"""
 * @apiGroup Proyecto
 * @api {get} /projectModule/projects/ Recupera todos los proyectos
 * @apiDescription Recuperar todos los proyectos con respecto a un usuario, es necesario estar autenticado
 * @apiHeader {String} Autorization JWT TOKEN
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "": "",
 *       "": ""
 *     }
"""

"""
 * @apiGroup Proyecto
 * @api {get} /projectModule/projects/:id_project Recupera un proyecto
 * @apiDescription Recuperar un proyecto especifico mediante su id, es necesario estar autenticado
 * @apiHeader {String} Autorization JWT TOKEN
 * @apiParam {String} id_project Id del proyecto a recuperar
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "": "",
 *       "": ""
 *     }
"""

class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ListOrRetrieveProjectSerializer

    def create(self, req):
        serializer = CreateProjectSerializer(data=req.data)
        if serializer.is_valid():
            saved_project = serializer.save()

            # Obtenemos todos los permisos a nivel de proyecto
            permissions = ProjectPermission.objects.all()

            #Automáticamente se crea un rol scrum master con todos los permisos
            role = ProjectRole.objects.create(
                project = saved_project,
                name = "Scrum master",
                description = "Tiene control total sobre el proyecto",
            )

            for permission in permissions:
                role.permissions.add(permission)

            role.save()

            # Automáticamente se crea un miembro
            Member.objects.create(
                user = req.user,
                project = saved_project,
                projectRole = role,
                availability = 8,
            )

            saved_serializer = ListOrRetrieveProjectSerializer(saved_project)
            return Response(data=saved_serializer.data, status=status.HTTP_201_CREATED)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def list(self,req):
        #Obtenemos el ID del Usuario
        usuario = req.user

        # Obtenemos todos los members que hacen referencia al user
        miembros = usuario.member_set.all()

        # Obtenemos todos los proyectos del sistema
        proyectos = Project.objects.all()
        
        proyectos_usuario = []

        for proyecto in proyectos:
            if proyecto.scrum_master.id == usuario.id:
                proyectos_usuario.append(proyecto)
            else:
                miembros_proyecto = proyecto.member_set.all()
                for miembro in miembros:
                    if miembro in miembros_proyecto:
                        proyectos_usuario.append(proyecto)
            
        
        #Traemos todos los proyectos
        serializer = self.serializer_class(proyectos_usuario,many=True)

        return Response(data=serializer.data,status=status.HTTP_200_OK)

class CrearProyectoView(views.APIView):

    def post(self, req):
        serializer = CreateProjectSerializer(data=req.data)
        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)