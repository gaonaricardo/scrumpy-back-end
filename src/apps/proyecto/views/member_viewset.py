from rest_framework import  status, views
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from proyecto.models.project import Project
from proyecto.models.member import Member

from proyecto.serializers import (
    CreateMemberSerializer, 
    ListOrRetrieveMemberSerializer,
    UpdateMemberSerializer,
    )
from scrumpy_backend.utils import enviar_email


"""
 * @apiGroup Miembros de un proyecto
 * @api {post} /projectModule/projects/:id_project/members/ Crear un miembro
 * @apiDescription Agrega un miembro a un proyecto
 * @apiParam {Number} id_project id del proyecto
 * @apiBody {String} user Email del usuario a agregar
 * @apiBody {Number} projectRole id de projectRole asignado
 * @apiBody {Boolean} is_active Miembro como oyente o no
 * @apiBody {Number} availability Disponibilidad en el proyecto con respecto al rol
 * @apiExample {json} Example body:
{
    "user":"mateogiulliano@hotmail.com",
    "projectRole": 1,
    "is_active": true,
    "availability": 2.0
}
 * @apiSuccessExample {json} Success-Response:
    HTTP/1.1 201 OK -  Devuelve el usuario creado
{
    "id": 2,
    "user": {
        "id": 1,
        "email": "mateogiulliano@hotmail.com",
        "first_name": "Giulliano",
        "last_name": "Albrecht",
        "address": "",
        "phone_number": "",
        "system_role": 1,
        "is_active": true
    },
    "project": {
        "id": 1,
        "scrum_master": {
            "id": 1,
            "email": "mateogiulliano@hotmail.com",
            "first_name": "Giulliano",
            "last_name": "Albrecht",
            "address": "",
            "phone_number": "",
            "system_role": 1,
            "is_active": true
        },
        "name": "Proyecto de Prueba",
        "description": "",
        "status": "INICIADO",
        "startDay": "2021-09-01",
        "endDay": "2021-09-14"
    },
    "projectRole": {
        "id": 1,
        "project": {
            "id": 1,
            "scrum_master": {
                "id": 1,
                "email": "mateogiulliano@hotmail.com",
                "first_name": "Giulliano",
                "last_name": "Albrecht",
                "address": "",
                "phone_number": "",
                "system_role": 1,
                "is_active": true
            },
            "name": "Proyecto de Prueba",
            "description": "",
            "status": "INICIADO",
            "startDay": "2021-09-01",
            "endDay": "2021-09-14"
        },
        "permissions": [
            1
        ],
        "name": "Developer",
        "description": ""
    },
    "is_active": true,
    "availability": 2.0
}
"""

"""
 * @apiGroup Miembros de un proyecto
 * @api {get} /projectModule/projects/:id_project/members/ Recuperar todos los miembros
 * @apiDescription Recuperar todos los miembros con respecto a un proyecto
 * @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
[
    {
        "id": 1,
        "user": {
            "id": 1,
            "email": "mateogiulliano@hotmail.com",
            "first_name": "Giulliano",
            "last_name": "Albrecht",
            "address": "",
            "phone_number": "",
            "system_role": 1,
            "is_active": true
        },
        "project": {
            "id": 1,
            "scrum_master": {
                "id": 1,
                "email": "mateogiulliano@hotmail.com",
                "first_name": "Giulliano",
                "last_name": "Albrecht",
                "address": "",
                "phone_number": "",
                "system_role": 1,
                "is_active": true
            },
            "name": "Proyecto de Prueba",
            "description": "",
            "status": "INICIADO",
            "startDay": "2021-09-01",
            "endDay": "2021-09-14"
        },
        "projectRole": {
            "id": 1,
            "project": {
                "id": 1,
                "scrum_master": {
                    "id": 1,
                    "email": "mateogiulliano@hotmail.com",
                    "first_name": "Giulliano",
                    "last_name": "Albrecht",
                    "address": "",
                    "phone_number": "",
                    "system_role": 1,
                    "is_active": true
                },
                "name": "Proyecto de Prueba",
                "description": "",
                "status": "INICIADO",
                "startDay": "2021-09-01",
                "endDay": "2021-09-14"
            },
            "permissions": [
                1
            ],
            "name": "Developer",
            "description": ""
        },
        "is_active": true,
        "availability": 2.0
    },
    {
        "id": 2,
        "user": {
            "id": 1,
            "email": "mateogiulliano@hotmail.com",
            "first_name": "Giulliano",
            "last_name": "Albrecht",
            "address": "",
            "phone_number": "",
            "system_role": 1,
            "is_active": true
        },
        "project": {
            "id": 1,
            "scrum_master": {
                "id": 1,
                "email": "mateogiulliano@hotmail.com",
                "first_name": "Giulliano",
                "last_name": "Albrecht",
                "address": "",
                "phone_number": "",
                "system_role": 1,
                "is_active": true
            },
            "name": "Proyecto de Prueba",
            "description": "",
            "status": "INICIADO",
            "startDay": "2021-09-01",
            "endDay": "2021-09-14"
        },
        "projectRole": {
            "id": 1,
            "project": {
                "id": 1,
                "scrum_master": {
                    "id": 1,
                    "email": "mateogiulliano@hotmail.com",
                    "first_name": "Giulliano",
                    "last_name": "Albrecht",
                    "address": "",
                    "phone_number": "",
                    "system_role": 1,
                    "is_active": true
                },
                "name": "Proyecto de Prueba",
                "description": "",
                "status": "INICIADO",
                "startDay": "2021-09-01",
                "endDay": "2021-09-14"
            },
            "permissions": [
                1
            ],
            "name": "Developer",
            "description": ""
        },
        "is_active": true,
        "availability": 2.0
    }
]
"""

"""
 * @apiGroup Miembros de un proyecto
 * @api {get} /projectModule/projects/:id_project/members/:id_member/ Recuperar un miembro
 * @apiDescription Recupera un miembro con respecto a un proyecto con su id_rol
 * @apiParam {Number} id_project id del proyecto
 * @apiParam {Number} id_member id del miembro
 * @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
{
    "id": 1,
    "user": {
        "id": 1,
        "email": "mateogiulliano@hotmail.com",
        "first_name": "Giulliano",
        "last_name": "Albrecht",
        "address": "",
        "phone_number": "",
        "system_role": 1,
        "is_active": true
    },
    "project": {
        "id": 1,
        "scrum_master": {
            "id": 1,
            "email": "mateogiulliano@hotmail.com",
            "first_name": "Giulliano",
            "last_name": "Albrecht",
            "address": "",
            "phone_number": "",
            "system_role": 1,
            "is_active": true
        },
        "name": "Proyecto de Prueba",
        "description": "",
        "status": "INICIADO",
        "startDay": "2021-09-01",
        "endDay": "2021-09-14"
    },
    "projectRole": {
        "id": 1,
        "project": {
            "id": 1,
            "scrum_master": {
                "id": 1,
                "email": "mateogiulliano@hotmail.com",
                "first_name": "Giulliano",
                "last_name": "Albrecht",
                "address": "",
                "phone_number": "",
                "system_role": 1,
                "is_active": true
            },
            "name": "Proyecto de Prueba",
            "description": "",
            "status": "INICIADO",
            "startDay": "2021-09-01",
            "endDay": "2021-09-14"
        },
        "permissions": [
            1
        ],
        "name": "Developer",
        "description": ""
    },
    "is_active": true,
    "availability": 2.0
}
"""

"""
 * @apiGroup Miembros de un proyecto
 * @api {put} /projectModule/projects/:id_project/members/:id_member/ Actualizar un miembro
 * @apiDescription Actualiza un miembro
 * @apiParam {Number} id_project id del proyecto
 * @apiParam {Number} id_member id del miembro
 * @apiBody {Number} projectRole id de projectRole a actualizar
 * @apiBody {Boolean} is_active Miembro como oyente o no
 * @apiBody {Number} availability Disponibilidad en el proyecto a actualizar
 * @apiExample {json} Example body:
{
    "projectRole": 1,
    "is_active": true,
    "availability": 25.0
}
 * @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK - Devuelve el Member actualizado
{
    "id": 1,
    "user": {
        "id": 1,
        "email": "mateogiulliano@hotmail.com",
        "first_name": "Giulliano",
        "last_name": "Albrecht",
        "address": "",
        "phone_number": "",
        "system_role": 1,
        "is_active": true
    },
    "project": {
        "id": 1,
        "scrum_master": {
            "id": 1,
            "email": "mateogiulliano@hotmail.com",
            "first_name": "Giulliano",
            "last_name": "Albrecht",
            "address": "",
            "phone_number": "",
            "system_role": 1,
            "is_active": true
        },
        "name": "Proyecto de Prueba",
        "description": "",
        "status": "INICIADO",
        "startDay": "2021-09-01",
        "endDay": "2021-09-14"
    },
    "projectRole": {
        "id": 1,
        "project": {
            "id": 1,
            "scrum_master": {
                "id": 1,
                "email": "mateogiulliano@hotmail.com",
                "first_name": "Giulliano",
                "last_name": "Albrecht",
                "address": "",
                "phone_number": "",
                "system_role": 1,
                "is_active": true
            },
            "name": "Proyecto de Prueba",
            "description": "",
            "status": "INICIADO",
            "startDay": "2021-09-01",
            "endDay": "2021-09-14"
        },
        "permissions": [
            1
        ],
        "name": "Developer",
        "description": ""
    },
    "is_active": true,
    "availability": 25.0
}
"""

"""
 * @apiGroup Miembros de un proyecto
 * @api {patch} /projectModule/projects/:id_project/members/:id_member/ Actualizar parcialmente un miembro
 * @apiDescription Actualiza parcialmente un miembro
 * @apiParam {Number} id_project id del proyecto
 * @apiParam {Number} id_member id del miembro
 * @apiBody {Number} projectRole id de projectRole a actualizar
 * @apiBody {Boolean} is_active Miembro como oyente o no
 * @apiBody {Number} availability Disponibilidad en el proyecto a actualizar
 * @apiExample {json} Example body:
{
    "projectRole": 1,
    "is_active": true,
    "availability": 25.0
}
 * @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK - Devuelve el Member actualizado
{
    "id": 1,
    "user": {
        "id": 1,
        "email": "mateogiulliano@hotmail.com",
        "first_name": "Giulliano",
        "last_name": "Albrecht",
        "address": "",
        "phone_number": "",
        "system_role": 1,
        "is_active": true
    },
    "project": {
        "id": 1,
        "scrum_master": {
            "id": 1,
            "email": "mateogiulliano@hotmail.com",
            "first_name": "Giulliano",
            "last_name": "Albrecht",
            "address": "",
            "phone_number": "",
            "system_role": 1,
            "is_active": true
        },
        "name": "Proyecto de Prueba",
        "description": "",
        "status": "INICIADO",
        "startDay": "2021-09-01",
        "endDay": "2021-09-14"
    },
    "projectRole": {
        "id": 1,
        "project": {
            "id": 1,
            "scrum_master": {
                "id": 1,
                "email": "mateogiulliano@hotmail.com",
                "first_name": "Giulliano",
                "last_name": "Albrecht",
                "address": "",
                "phone_number": "",
                "system_role": 1,
                "is_active": true
            },
            "name": "Proyecto de Prueba",
            "description": "",
            "status": "INICIADO",
            "startDay": "2021-09-01",
            "endDay": "2021-09-14"
        },
        "permissions": [
            1
        ],
        "name": "Developer",
        "description": ""
    },
    "is_active": true,
    "availability": 25.0
}
"""

"""
 * @apiGroup Miembros de un proyecto
 * @api {delete} /projectModule/projects/:id_project/members/:id_member/ Eliminar un miembro
 * @apiDescription Elimina un miembro con respecto a un proyecto con su id_member
 * @apiParam {Number} id_project id del proyecto
 * @apiParam {Number} id_member id del miembro
 * @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK - None
"""

class MemberView(views.APIView):
    def post(self, req, id_project):
        serializer = CreateMemberSerializer(data=req.data)
        if serializer.is_valid():
            member = serializer.save()
            project = Project.objects.get(pk=id_project)
            member.project = project
            member.save()
            """
            enviar_email(
                subject="Notificaciones del proyecto",
                content="Se te ha agregado al proyecto: {}\n".format(project.name),
                email_to=member.user.email
            )
            """
            new_serializer = ListOrRetrieveMemberSerializer(member)
            return Response(data=new_serializer.data, status=status.HTTP_201_CREATED)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def get(self, req, id_project):
        members = Member.objects.filter(project=id_project)
        serializer = ListOrRetrieveMemberSerializer(members,many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class MemberRetrieveView(views.APIView):
    def get(self, req, id_project, id_member):
        member = get_object_or_404(Member, pk=id_member)
        serializer = ListOrRetrieveMemberSerializer(member)
        return Response(data=serializer.data, status=status.HTTP_200_OK)
    
    def put(self, req, id_project, id_member):
        serializer = UpdateMemberSerializer(data=req.data)
        member = get_object_or_404(Member, pk=id_member)
        if serializer.is_valid():
            updated_member = serializer.update(member, serializer.validated_data)
            updated_serializer = ListOrRetrieveMemberSerializer(updated_member)
            return Response(data=updated_serializer.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def patch(self, req, id_project, id_member):
        serializer = UpdateMemberSerializer(data=req.data, partial=True)
        member = get_object_or_404(Member, pk=id_member)
        if serializer.is_valid():
            updated_member = serializer.update(member, serializer.validated_data)
            updated_serializer = ListOrRetrieveMemberSerializer(updated_member)
            return Response(data=updated_serializer.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self, req, id_project, id_member):
        member = get_object_or_404(Member, pk=id_member)
        member.delete()
        return Response(data=None, status=status.HTTP_200_OK)