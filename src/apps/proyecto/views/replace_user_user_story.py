from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from autenticacion.models.user import CustomUser
from sprint.models.user_story import UserStory 
from sprint.serializers.user_story_serializers import ListOrRetrieveUserStorySerializer
from proyecto.models import Member

"""
 * @apiGroup User story
 * @api {post} /projectModule/projects/<int:id_project>/replace/  Reemplazar US de un Miembro
 * @apiDescription Reemplazar Todos los User Stories de un Miembro con otro Usuario
 * @apiHeader {String} Autorization JWT TOKEN
 * @apiParam {String} id_project id del proyecto
 * @apiBody {String} id_old_member Id del Miembro que sera reemplazado
 * @apiBody {String} id_new_member Id del Miembro que sera el reemplazo
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 2,
 *              "project": {
 *              "id": 1,
 *              "scrum_master": {
 *                      "id": 1,
 *                      "email": "ramirezmatias946@gmail.com",
 *                      "first_name": "Matias",
 *                      "last_name": "Ramirez",
 *                      "address": "",
 *                      "phone_number": "",
 *                      "system_role": 1,
 *                      "is_active": true,
 *                      "picture": "https://lh3.googleusercontent.com/a-/AOh14GjKkIKo3RU-3K3-fjUYimW-K7_74aYOMe9POiCZcQ=s96-c"
 *              },
 *              "name": "Proyecto 1",
 *              "description": "Descripcion Proyecto 1",
 *              "status": "INICIADO",
 *              "startDay": "2021-10-28",
 *              "endDay": "2021-10-27",
 *              "sprintDuration": 1
 *            },
 *            "sprint": null,
 *            "name": "US2",
 *            "description": "DUS2",
 *            "priority": 9,
 *            "estimate": 8,
 *            "worked_hours": null,
 *            "status": "NEW",
 *            "member": 2
 *         },
 *         {
 *              "id": 3,
 *              "project": {
 *                  "id": 1,
 *                  "scrum_master": {
 *                      "id": 1,
 *                      "email": "ramirezmatias946@gmail.com",
 *                      "first_name": "Matias",
 *                      "last_name": "Ramirez",
 *                      "address": "",
 *                      "phone_number": "",
 *                      "system_role": 1,
 *                      "is_active": true,
 *                      "picture": "https://lh3.googleusercontent.com/a-/AOh14GjKkIKo3RU-3K3-fjUYimW-K7_74aYOMe9POiCZcQ=s96-c"
 *                  },
 *                  "name": "Proyecto 1",
 *                  "description": "Descripcion Proyecto 1",
 *                  "status": "INICIADO",
 *                  "startDay": "2021-10-28",
 *                  "endDay": "2021-10-27",
 *                  "sprintDuration": 1
 *              },
 *              "sprint": null,
 *              "name": "US3",
 *              "description": "SDS",
 *              "priority": 6,
 *              "estimate": 6,
 *              "worked_hours": null,
 *              "status": "NEW",
 *              "member": 2
 *          }
 *      ]
"""

class ReplaceMemberUserStoryApiView(APIView):
    def post(self,req,id_project):
        
        #Usuario que sera reemplazado
        miembroReemplazado = Member.objects.get(pk=int(req.data['id_old_member']))
        
        #Usuario que va a reemplazar
        userReemplazo = CustomUser.objects.get(id=req.data['id_new_member'])
        
        project = id_project
        
        user_story_member_reemplazado = UserStory.objects.filter(member=miembroReemplazado,project=project)
        
        miembroReemplazado.user = userReemplazo

        miembroReemplazado.save()

        return Response(status=status.HTTP_200_OK,data=ListOrRetrieveUserStorySerializer(user_story_member_reemplazado,many=True).data)
        
    pass