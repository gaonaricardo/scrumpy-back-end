from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status, permissions

from proyecto.serializers import EmailSerializer
from scrumpy_backend.utils import enviar_email

class NotificadorAPI(APIView):
    def post(self, req):
        serializer = EmailSerializer(data=req.data)
        if serializer.is_valid():
            """
            enviar_email(
                subject=serializer.validated_data.get("subject"),
                email_to=serializer.validated_data.get("mail_to"),
                content=serializer.validated_data.get("content")
            )
            """
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)
