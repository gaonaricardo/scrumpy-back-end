from proyecto.models import ProjectPermission
from proyecto.serializers import ProjectPermissionSerializer
from rest_framework import viewsets, mixins

"""
 * @apiGroup Permisos de proyecto
 * @api {get} /projectModule/projects/permission/ Recuperar permisos
 * @apiDescription Recuperar todos los permisos a nivel de proyecto
 * @apiSuccessExample {json} Success-Response:
      HTTP/1.1 200 OK
[
    {
        "id": 1,
        "name": "Asginar esto",
        "description": "",
        "code": 1
    },
    {
        "id": 2,
        "name": "Asignar aquello",
        "description": "",
        "code": 2
    }
]
"""

class ProjectPermissionViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    queryset = ProjectPermission.objects.all()
    serializer_class = ProjectPermissionSerializer
