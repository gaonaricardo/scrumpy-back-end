from datetime import datetime,date

from rest_framework import  status, views, permissions
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from sprint.models import sprint
from autenticacion.models.system_permission import SystemPermission
from autenticacion.models.system_role import SystemRole


from autenticacion.models import CustomUser
from proyecto.models import Project, Member, ProjectPermission, ProjectRole
from sprint.models import UserStory, Activity, Sprint, PastSprints, EventoUserStory


class PoblarBDView(views.APIView):

    permission_classes = [permissions.AllowAny,]

    def crear_permisos_sistema(self):

        SystemPermission.objects.create(
            code=100, 
            name='Iniciar sesión', 
            description='Permite al usuario iniciar sesión en el sistema'
        )
        SystemPermission.objects.create(
            code = 200, 
            name='Registrar usuario',
            description='Permite al usuario iniciar sesión en el sistema'
        )
        SystemPermission.objects.create(
            code = 201, 
            name='Modificar usuario',
            description='Permite al usuario iniciar sesión en el sistema'
        )
        SystemPermission.objects.create(
            code = 300, 
            name='Crear proyecto',
            description='Permite al usuario iniciar sesión en el sistema'
        )
        SystemPermission.objects.create(
            code = 301, 
            name='Modificar proyecto',
            description='Permite al usuario iniciar sesión en el sistema'
        )
        SystemPermission.objects.create(
            code = 400, 
            name='Crear rol',
            description='Permite al usuario iniciar sesión en el sistema'
        )
        SystemPermission.objects.create(
            code = 401, 
            name='Modificar rol',
            description='Permite al usuario iniciar sesión en el sistema'
        )
        SystemPermission.objects.create(
            code = 402, 
            name='Eliminar rol',
            description='Permite al usuario iniciar sesión en el sistema'
        )
    
    def crear_permisos_proyecto(self):
        ProjectPermission.objects.create(
            code = 100, 
            name = 'Crear user story', 
            description = 'Permite al usuario agregar un nuevo user story'
        )
        ProjectPermission.objects.create(
            code = 101, 
            name = 'Modificar user story', 
            description = 'Permite al usuario modificar un user story existente'
        )
        ProjectPermission.objects.create(
            code = 102, 
            name = 'Asignar user story', 
            description = 'Permite al usuario asignar un responsable al user story'
        )
        ProjectPermission.objects.create(
            code = 103, 
            name = 'Estimar user story', 
            description = 'Permite al usuario agregar una estimación al user story'
        )
        ProjectPermission.objects.create(
            code = 104, 
            name = 'Archivar user story', 
            description = 'Permite al usuario archivar un user story'
        )
        ProjectPermission.objects.create(
            code = 200, 
            name = 'Crear sprint', 
            description = 'Permite al usuario crear un nuevo sprint'
        )
        ProjectPermission.objects.create(
            code = 201, 
            name = 'Planificar sprint', 
            description = 'Permite al usuario agregar user stories al sprint backlog'
        )
        ProjectPermission.objects.create(
            code = 202, 
            name = 'Iniciar sprint', 
            description = 'Permite al usuario iniciar un sprint planificado'
        )
        ProjectPermission.objects.create(
            code = 203, 
            name = 'Finalizar sprint', 
            description = 'Permite al usuario finalizar en cualquier momento un sprint'
        )
        ProjectPermission.objects.create(
            code = 300, 
            name = 'Agregar miembro', 
            description = 'Permite al usuario agregar un nuevo miembro al proyecto'
        )
        ProjectPermission.objects.create(
            code = 301, 
            name = 'Modificar miembro', 
            description = 'Permite al usuario modificar un miembro existente'
        )
        ProjectPermission.objects.create(
            code = 302, 
            name = 'Eliminar miembro', 
            description = 'Permite al usuario eliminar un miembro que no se encuentre en un sprint en curso'
        )
        ProjectPermission.objects.create(
            code = 400, 
            name = 'Crear rol de proyecto', 
            description = 'Permite al usuario crear un rol con permisos a nivel de proyecto'
        )
        ProjectPermission.objects.create(
            code = 401, 
            name = 'Modificar rol de proyecto', 
            description = 'Permite al usuario modificar detalles y permisos de un rol de proyecto'
        )
        ProjectPermission.objects.create(
            code = 500, 
            name = 'Editar las configuraciones', 
            description = 'Permite al usuario editar las configuraciones del proyecto'
        )

    def crear_rol_administrador(self):
        rol_admin = SystemRole.objects.create(
            name='Administrador',
            description='Tiene control total sobre el sistema'
        )

        permissions = SystemPermission.objects.all()

        for permission in permissions:
            rol_admin.permissions.add(permission)

        rol_admin.save()

        return rol_admin

    def crear_rol_empleado(self):
        rol_emp = SystemRole.objects.create(
            name='Empleado',
            description='Puede loguearse y trabajar en sus proyectos asignados'
        )

        permission = SystemPermission.objects.get(code=100)

        rol_emp.permissions.add(permission)

        rol_emp.save()

        return rol_emp

    def post(self, req):

        self.crear_permisos_sistema()
        self.crear_permisos_proyecto()

        rol_admin = self.crear_rol_administrador()
        rol_emp = self.crear_rol_empleado()

        """
        usuario1 = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan',
            system_role=rol_admin
        )
        """

        usuario1 = CustomUser.objects.create(
            email='is2equipo10@gmail.com',
            first_name='Equipo',
            last_name='Diez',
            system_role=rol_admin
        )

        usuario2 = CustomUser.objects.create(
            email='gaonaricardo06@gmail.com',
            first_name='Ricardo',
            last_name='Gaona',
            system_role=rol_admin
        )

        usuario3 = CustomUser.objects.create(
            email='mgalbrecht20005@gmail.com',
            first_name='Giulliano',
            last_name='ALbrecht',
            system_role=rol_emp
        )

        usuario4 = CustomUser.objects.create(
            email='ramirezmatias946@gmail.com',
            first_name='Matias',
            last_name='Ramirez',
            system_role=rol_emp
        )

        usuario5 = CustomUser.objects.create(
            email='ramirez.swag23@gmail.com',
            first_name='SebastianSwag',
            last_name='Ramirez sin swag',
            system_role=rol_emp
        )

        # Creamos un proyecto
        proyecto1 = Project.objects.create(
            name='Proyecto 1',
            description='Este es el proyecto recien creado',
            scrum_master=usuario2,
            status='INICIADO',
            startDay=date(2021, 11, 17).strftime('%Y-%m-%d'),
            endDay=date(2021, 12, 24).strftime('%Y-%m-%d')
        )

        # Obtenemos todos los permisos a nivel de proyecto
        permissions = ProjectPermission.objects.all()

        #Automáticamente se crea un rol scrum master con todos los permisos
        role = ProjectRole.objects.create(
            project = proyecto1,
            name = "Scrum master",
            description = "Tiene control total sobre el proyecto",
        )

        for permission in permissions:
            role.permissions.add(permission)

        role.save()

        # Automáticamente se crea un miembro
        miembro2 = Member.objects.create(
            user = usuario2,
            project = proyecto1,
            projectRole = role,
            availability = 8,
        )

        # Creamos un rol developer
        rol_dev = ProjectRole.objects.create(
            project = proyecto1,
            name = "Developer",
            description = "Se encarga de implementar los US",
        )

        permission = ProjectPermission.objects.get(code=101)

        rol_dev.permissions.add(permission)

        rol_dev.save()

        Member.objects.create(
            user = usuario5,
            project = proyecto1,
            projectRole = rol_dev,
            availability = 8,
        )

        miembro1 = Member.objects.create(
            user = usuario1,
            project = proyecto1,
            projectRole = rol_dev,
            availability = 8,
        )

        miembro3 = Member.objects.create(
            user = usuario3,
            project = proyecto1,
            projectRole = rol_dev,
            availability = 8,
        )

        miembro4 = Member.objects.create(
            user = usuario4,
            project = proyecto1,
            projectRole = rol_dev,
            availability = 8,
        )


        # Creamos un par de user stories
        user_s = UserStory.objects.create(
            project=proyecto1,
            name='Story 1',
            description='Esta es una historia de prueba',
            priority=3,
        )

        user_s2 = UserStory.objects.create(
            project=proyecto1,
            name='Story 2',
            description='Esta es otra historia de prueba',
            priority=7,
        )

        user_s3 = UserStory.objects.create(
            project=proyecto1,
            name='Story 3',
            description='Y una prueba mas',
            priority=2,
        )

        user_s4 = UserStory.objects.create(
            project=proyecto1,
            name='Story 4',
            description='Esta es una historia de prueba',
            priority=5,
        )

        user_s5 = UserStory.objects.create(
            project=proyecto1,
            name='Story 5',
            description='Esta es otra historia de prueba',
            priority=6, 
        )


        user_s6 = UserStory.objects.create(
            project=proyecto1,
            name='Story 6',
            description='Y una prueba mas',
            priority=1,
        )

        user_s7 = UserStory.objects.create(
            project=proyecto1,
            name='Story 7',
            description='Esta es una historia de prueba',
            priority=4,
        )

        user_s8 = UserStory.objects.create(
            project=proyecto1,
            name='Story 8',
            description='Esta es otra historia de prueba',
            priority=8,
        )

        user_s9 = UserStory.objects.create(
            project=proyecto1,
            name='Story 9',
            description='Y una prueba mas',
            priority=9,
        )

        EventoUserStory.objects.create(
            user_story=user_s, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 1, user_s.name, user_s.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s2, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 1, user_s2.name, user_s2.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s3, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 1, user_s3.name, user_s3.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s4, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 1, user_s4.name, user_s4.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s5, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 1, user_s5.name, user_s5.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s6, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 1, user_s6.name, user_s6.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s7, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 1, user_s7.name, user_s7.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s8, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 1, user_s8.name, user_s8.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s9, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 1, user_s9.name, user_s9.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )






        # Creamos un proyecto
        proyecto1 = Project.objects.create(
            name='Proyecto 2',
            description='Este es proyecto en curso',
            scrum_master=usuario2,
            status='INICIADO',
            startDay=date(2021, 11, 17).strftime('%Y-%m-%d'),
            endDay=date(2021, 12, 24).strftime('%Y-%m-%d')
        )

        # Obtenemos todos los permisos a nivel de proyecto
        permissions = ProjectPermission.objects.all()

        #Automáticamente se crea un rol scrum master con todos los permisos
        role = ProjectRole.objects.create(
            project = proyecto1,
            name = "Scrum master",
            description = "Tiene control total sobre el proyecto",
        )

        for permission in permissions:
            role.permissions.add(permission)

        role.save()

        # Automáticamente se crea un miembro
        miembro2 = Member.objects.create(
            user = usuario2,
            project = proyecto1,
            projectRole = role,
            availability = 8,
        )

        # Creamos un rol developer
        rol_dev = ProjectRole.objects.create(
            project = proyecto1,
            name = "Developer",
            description = "Se encarga de implementar los US",
        )

        permission = ProjectPermission.objects.get(code=101)

        rol_dev.permissions.add(permission)

        rol_dev.save()

        Member.objects.create(
            user = usuario5,
            project = proyecto1,
            projectRole = rol_dev,
            availability = 8,
        )

        miembro1 = Member.objects.create(
            user = usuario1,
            project = proyecto1,
            projectRole = rol_dev,
            availability = 8,
        )

        miembro3 = Member.objects.create(
            user = usuario3,
            project = proyecto1,
            projectRole = rol_dev,
            availability = 8,
        )

        miembro4 = Member.objects.create(
            user = usuario4,
            project = proyecto1,
            projectRole = rol_dev,
            availability = 8,
        )

        sprint1 = Sprint.objects.create(
            sprint_goal='Objetivo del sprint',
            project=proyecto1,
            status='FINALIZADO',
            iteration=1,
            startDay=date(2021, 11, 18).strftime('%Y-%m-%d'),
            endDay=date(2021, 11, 24).strftime('%Y-%m-%d')
        )


        # Creamos un par de user stories
        user_s = UserStory.objects.create(
            project=proyecto1,
            sprint=sprint1,
            name='Story 1',
            description='Esta es una historia de prueba',
            priority=3,
            estimate=10,
            member=miembro1,
            finish_date=date(2021, 11, 20).strftime('%Y-%m-%d'),
            status="DONE",
            worked_hours=10,
        )

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s,
            story_status = user_s.status,
            priority = user_s.priority,
            member = user_s.member,
            estimate = user_s.estimate,
            worked_hours = user_s.worked_hours,
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario1,
            created=date(2021, 11, 18).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s,
            description = 'Contenido',
            worked_hours = 3,
            user=usuario1,
            created=date(2021, 11, 19).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s,
            description = 'Contenido',
            worked_hours = 3,
            user=usuario1,
            created=date(2021, 11, 20).strftime('%Y-%m-%d'),
        )

        user_s.status = "RELEASE"
        user_s.save()

        user_s2 = UserStory.objects.create(
            project=proyecto1,
            name='Story 2',
            description='Esta es otra historia de prueba',
            priority=7,
            sprint=sprint1,
            estimate=10,
            member=miembro1,
            finish_date=date(2021, 11, 23).strftime('%Y-%m-%d'),
            status="DOING",
            worked_hours=12,
        )

        user_s.status = "RELEASE"
        user_s.save()

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s2,
            story_status = user_s2.status,
            priority = user_s2.priority,
            member = user_s2.member,
            estimate = user_s2.estimate,
            worked_hours = user_s2.worked_hours,
        )

        user_s2.status = "REJECTED"
        user_s2.priority=10
        user_s2.save()

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s2,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario1,
            created=date(2021, 11, 21).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s2,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario1,
            created=date(2021, 11, 22).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s2,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario1,
            created=date(2021, 11, 23).strftime('%Y-%m-%d'),
        )

        user_s3 = UserStory.objects.create(
            project=proyecto1,
            name='Story 3',
            description='Y una prueba mas',
            priority=2,
            sprint=sprint1,
            estimate=8,
            member=miembro2,
            finish_date=date(2021, 11, 23).strftime('%Y-%m-%d'),
            status="DONE",
            worked_hours=9,
        )
        

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s3,
            story_status = user_s3.status,
            priority = user_s3.priority,
            member = user_s3.member,
            estimate = user_s3.estimate,
            worked_hours = user_s3.worked_hours,
        )

        user_s3.status = "RELEASE"
        user_s3.save()

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s3,
            description = 'Contenido',
            worked_hours = 5,
            user=usuario2,
            created=date(2021, 11, 22).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s3,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario2,
            created=date(2021, 11, 23).strftime('%Y-%m-%d'),
        )

        user_s4 = UserStory.objects.create(
            project=proyecto1,
            name='Story 4',
            description='Esta es una historia de prueba',
            priority=5,
            sprint=sprint1,
            estimate=12,
            member=miembro3,
            finish_date=date(2021, 11, 24).strftime('%Y-%m-%d'),
            status="DOING",
            worked_hours=12,
            
        )

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s4,
            story_status = user_s4.status,
            priority = user_s4.priority,
            member = user_s4.member,
            estimate = user_s4.estimate,
            worked_hours = user_s4.worked_hours,
        )

        user_s4.status = "REJECTED"
        user_s4.priority=10
        user_s4.save()

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s4,
            description = 'Contenido',
            worked_hours = 5,
            user=usuario3,
            created=date(2021, 11, 18).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s4,
            description = 'Contenido',
            worked_hours = 7,
            user=usuario3,
            created=date(2021, 11, 24).strftime('%Y-%m-%d'),
        )

        user_s5 = UserStory.objects.create(
            project=proyecto1,
            name='Story 5',
            description='Esta es otra historia de prueba',
            priority=6,
            sprint=sprint1,
            estimate=7,
            member=miembro4,
            finish_date=date(2021, 11, 24).strftime('%Y-%m-%d'),
            status="DONE",
            worked_hours=7,
            
        )

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s5,
            story_status = user_s5.status,
            priority = user_s5.priority,
            member = user_s5.member,
            estimate = user_s5.estimate,
            worked_hours = user_s5.worked_hours,
        )

        user_s5.status = "RELEASE"
        user_s5.save()



        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s5,
            description = 'Contenido',
            worked_hours = 3,
            user=usuario3,
            created=date(2021, 11, 20).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s5,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario3,
            created=date(2021, 11, 24).strftime('%Y-%m-%d'),
        )

        
        sprint1 = Sprint.objects.create(
            sprint_goal='Objetivo del sprint',
            project=proyecto1,
            status='EN_PLANIFICACION',
            iteration=2,
        )

        user_s6 = UserStory.objects.create(
            project=proyecto1,
            name='Story 6',
            description='Y una prueba mas',
            priority=1,
            sprint=sprint1,
            estimate=12,
            member=miembro1,
            status="NEW",
            worked_hours=0,
        )


        #user_s6.status = "RELEASE"
        #user_s6.save()

        user_s7 = UserStory.objects.create(
            project=proyecto1,
            name='Story 7',
            description='Esta es una historia de prueba',
            priority=4,
            sprint=sprint1,
            estimate=10,
            member=miembro2,
            status="NEW",
            worked_hours=0,
        )


        #user_s7.status = "RELEASE"
        #user_s7.save()


        user_s8 = UserStory.objects.create(
            project=proyecto1,
            name='Story 8',
            description='Esta es otra historia de prueba',
            priority=8,
            sprint=sprint1,
            estimate=7,
            member=miembro3,            
            status="NEW",
            worked_hours=0,
        )


        #user_s8.status = "RELEASE"
        #user_s8.save()

        user_s9 = UserStory.objects.create(
            project=proyecto1,
            name='Story 9',
            description='Y una prueba mas',
            priority=9,
            sprint=sprint1,
            estimate=7,
            member=miembro4,
            status="NEW",
            worked_hours=0,
        )

        #user_s9.status = "RELEASE"
        #user_s9.save()

        EventoUserStory.objects.create(
            user_story=user_s, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s.name, user_s.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s2, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s2.name, user_s2.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s3, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s3.name, user_s3.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s4, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s4.name, user_s4.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s5, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s5.name, user_s5.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s6, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s6.name, user_s6.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s7, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s7.name, user_s7.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s8, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s8.name, user_s8.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s9, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s9.name, user_s9.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s.name, user_s.description, 'TODO'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s2, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s2.name, user_s2.description, 'TODO'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s3, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s3.name, user_s3.description, 'TODO'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s4, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s4.name, user_s4.description, 'TODO'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s5, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s5.name, user_s5.description, 'TODO'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s, 
            user=usuario1, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario1.email, 2, user_s.name, user_s.description, 'DOING'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s2, 
            user=usuario1, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario1.email, 2, user_s2.name, user_s2.description, 'DOING'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s3, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s3.name, user_s3.description, 'DOING'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s4, 
            user=usuario3, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario3.email, 2, user_s4.name, user_s4.description, 'DOING'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s5, 
            user=usuario4, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario4.email, 2, user_s5.name, user_s5.description, 'DOING'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s, 
            user=usuario1, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario1.email, 2, user_s.name, user_s.description, 'DONE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s2, 
            user=usuario1, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario1.email, 2, user_s2.name, user_s2.description, 'DONE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s3, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s3.name, user_s3.description, 'DONE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s4, 
            user=usuario3, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario3.email, 2, user_s4.name, user_s4.description, 'DONE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s5, 
            user=usuario4, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario4.email, 2, user_s5.name, user_s5.description, 'DONE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s.name, user_s.description, 'TESTING'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s2, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s2.name, user_s2.description, 'TESTING'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s3, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s3.name, user_s3.description, 'TESTING'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s4, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s4.name, user_s4.description, 'TESTING'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s5, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s5.name, user_s5.description, 'TESTING'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s.name, user_s.description, 'RELEASE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s2, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s2.name, user_s2.description, 'REJECTED'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s3, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s3.name, user_s3.description, 'RELEASE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s4, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s4.name, user_s4.description, 'REJECTED'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s5, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s5.name, user_s5.description, 'RELEASE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )



        # Creamos un proyecto
        proyecto1 = Project.objects.create(
            name='Proyecto 3',
            description='Este es el proyecto finalizado',
            scrum_master=usuario2,
            status='FINALIZADO',
            startDay=date(2021, 11, 17).strftime('%Y-%m-%d'),
            endDay=date(2021, 12, 2).strftime('%Y-%m-%d')
        )

        # Obtenemos todos los permisos a nivel de proyecto
        permissions = ProjectPermission.objects.all()

        #Automáticamente se crea un rol scrum master con todos los permisos
        role = ProjectRole.objects.create(
            project = proyecto1,
            name = "Scrum master",
            description = "Tiene control total sobre el proyecto",
        )

        for permission in permissions:
            role.permissions.add(permission)

        role.save()

        # Automáticamente se crea un miembro
        miembro2 = Member.objects.create(
            user = usuario2,
            project = proyecto1,
            projectRole = role,
            availability = 8,
        )

        # Creamos un rol developer
        rol_dev = ProjectRole.objects.create(
            project = proyecto1,
            name = "Developer",
            description = "Se encarga de implementar los US",
        )

        permission = ProjectPermission.objects.get(code=101)

        rol_dev.permissions.add(permission)

        rol_dev.save()

        Member.objects.create(
            user = usuario5,
            project = proyecto1,
            projectRole = rol_dev,
            availability = 8,
        )

        miembro1 = Member.objects.create(
            user = usuario1,
            project = proyecto1,
            projectRole = rol_dev,
            availability = 8,
        )

        miembro3 = Member.objects.create(
            user = usuario3,
            project = proyecto1,
            projectRole = rol_dev,
            availability = 8,
        )

        miembro4 = Member.objects.create(
            user = usuario4,
            project = proyecto1,
            projectRole = rol_dev,
            availability = 8,
        )

        sprint1 = Sprint.objects.create(
            sprint_goal='Objetivo del sprint',
            project=proyecto1,
            status='FINALIZADO',
            iteration=1,
            startDay=date(2021, 11, 18).strftime('%Y-%m-%d'),
            endDay=date(2021, 11, 24).strftime('%Y-%m-%d')
        )


        # Creamos un par de user stories
        user_s = UserStory.objects.create(
            project=proyecto1,
            sprint=sprint1,
            name='Story 1',
            description='Esta es una historia de prueba',
            priority=3,
            estimate=10,
            member=miembro1,
            finish_date=date(2021, 11, 20).strftime('%Y-%m-%d'),
            status="DONE",
            worked_hours=10,
        )

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s,
            story_status = user_s.status,
            priority = user_s.priority,
            member = user_s.member,
            estimate = user_s.estimate,
            worked_hours = user_s.worked_hours,
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario1,
            created=date(2021, 11, 18).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s,
            description = 'Contenido',
            worked_hours = 3,
            user=usuario1,
            created=date(2021, 11, 19).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s,
            description = 'Contenido',
            worked_hours = 3,
            user=usuario1,
            created=date(2021, 11, 20).strftime('%Y-%m-%d'),
        )

        user_s.status = "RELEASE"
        user_s.save()

        user_s2 = UserStory.objects.create(
            project=proyecto1,
            name='Story 2',
            description='Esta es otra historia de prueba',
            priority=7,
            sprint=sprint1,
            estimate=10,
            member=miembro1,
            finish_date=date(2021, 11, 23).strftime('%Y-%m-%d'),
            status="DOING",
            worked_hours=12,
        )

        user_s.status = "RELEASE"
        user_s.save()

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s2,
            story_status = user_s2.status,
            priority = user_s2.priority,
            member = user_s2.member,
            estimate = user_s2.estimate,
            worked_hours = user_s2.worked_hours,
        )

        user_s2.status = "REJECTED"
        user_s2.priority=10
        user_s2.save()

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s2,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario1,
            created=date(2021, 11, 21).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s2,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario1,
            created=date(2021, 11, 22).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s2,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario1,
            created=date(2021, 11, 23).strftime('%Y-%m-%d'),
        )

        user_s3 = UserStory.objects.create(
            project=proyecto1,
            name='Story 3',
            description='Y una prueba mas',
            priority=2,
            sprint=sprint1,
            estimate=8,
            member=miembro2,
            finish_date=date(2021, 11, 23).strftime('%Y-%m-%d'),
            status="DONE",
            worked_hours=9,
        )
        

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s3,
            story_status = user_s3.status,
            priority = user_s3.priority,
            member = user_s3.member,
            estimate = user_s3.estimate,
            worked_hours = user_s3.worked_hours,
        )

        user_s3.status = "RELEASE"
        user_s3.save()

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s3,
            description = 'Contenido',
            worked_hours = 5,
            user=usuario2,
            created=date(2021, 11, 22).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s3,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario2,
            created=date(2021, 11, 23).strftime('%Y-%m-%d'),
        )

        user_s4 = UserStory.objects.create(
            project=proyecto1,
            name='Story 4',
            description='Esta es una historia de prueba',
            priority=5,
            sprint=sprint1,
            estimate=12,
            member=miembro3,
            finish_date=date(2021, 11, 24).strftime('%Y-%m-%d'),
            status="DOING",
            worked_hours=12,
            
        )

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s4,
            story_status = user_s4.status,
            priority = user_s4.priority,
            member = user_s4.member,
            estimate = user_s4.estimate,
            worked_hours = user_s4.worked_hours,
        )

        user_s4.status = "REJECTED"
        user_s4.priority=10
        user_s4.save()

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s4,
            description = 'Contenido',
            worked_hours = 5,
            user=usuario3,
            created=date(2021, 11, 18).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s4,
            description = 'Contenido',
            worked_hours = 7,
            user=usuario3,
            created=date(2021, 11, 24).strftime('%Y-%m-%d'),
        )

        user_s5 = UserStory.objects.create(
            project=proyecto1,
            name='Story 5',
            description='Esta es otra historia de prueba',
            priority=6,
            sprint=sprint1,
            estimate=7,
            member=miembro4,
            finish_date=date(2021, 11, 24).strftime('%Y-%m-%d'),
            status="DONE",
            worked_hours=7,
            
        )

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s5,
            story_status = user_s5.status,
            priority = user_s5.priority,
            member = user_s5.member,
            estimate = user_s5.estimate,
            worked_hours = user_s5.worked_hours,
        )

        user_s5.status = "RELEASE"
        user_s5.save()



        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s5,
            description = 'Contenido',
            worked_hours = 3,
            user=usuario3,
            created=date(2021, 11, 20).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s5,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario3,
            created=date(2021, 11, 24).strftime('%Y-%m-%d'),
        )


        sprint1 = Sprint.objects.create(
            sprint_goal='Objetivo del sprint',
            project=proyecto1,
            status='FINALIZADO',
            iteration=2,
            startDay=date(2021, 11, 25).strftime('%Y-%m-%d'),
            endDay=date(2021, 12, 1).strftime('%Y-%m-%d')
        )


        user_s6 = UserStory.objects.create(
            project=proyecto1,
            name='Story 6',
            description='Y una prueba mas',
            priority=1,
            sprint=sprint1,
            estimate=12,
            member=miembro1,
            finish_date=date(2021, 11, 26).strftime('%Y-%m-%d'),
            status="DONE",
            worked_hours=12,
        )

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s6,
            story_status = user_s6.status,
            priority = user_s6.priority,
            member = user_s6.member,
            estimate = user_s6.estimate,
            worked_hours = user_s6.worked_hours,
        )

        user_s6.status = "RELEASE"
        user_s6.save()

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s6,
            description = 'Contenido',
            worked_hours = 5,
            user=usuario1,
            created=date(2021, 11, 25).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s6,
            description = 'Contenido',
            worked_hours = 7,
            user=usuario1,
            created=date(2021, 11, 26).strftime('%Y-%m-%d'),
        )

        user_s7 = UserStory.objects.create(
            project=proyecto1,
            name='Story 7',
            description='Esta es una historia de prueba',
            priority=4,
            sprint=sprint1,
            estimate=10,
            member=miembro2,
            finish_date=date(2021, 11, 30).strftime('%Y-%m-%d'),
            status="DONE",
            worked_hours=10,
        )

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s7,
            story_status = user_s7.status,
            priority = user_s7.priority,
            member = user_s7.member,
            estimate = user_s7.estimate,
            worked_hours = user_s7.worked_hours,
        )

        user_s7.status = "RELEASE"
        user_s7.save()

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s7,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario2,
            created=date(2021, 11, 29).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s7,
            description = 'Contenido',
            worked_hours = 7,
            user=usuario2,
            created=date(2021, 11, 30).strftime('%Y-%m-%d'),
        )

        user_s8 = UserStory.objects.create(
            project=proyecto1,
            name='Story 8',
            description='Esta es otra historia de prueba',
            priority=8,
            sprint=sprint1,
            estimate=7,
            member=miembro3,
            finish_date=date(2021, 12, 1).strftime('%Y-%m-%d'),
            status="DONE",
            worked_hours=7,
        )

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s8,
            story_status = user_s8.status,
            priority = user_s8.priority,
            member = user_s8.member,
            estimate = user_s8.estimate,
            worked_hours = user_s8.worked_hours,
        )

        user_s8.status = "RELEASE"
        user_s8.save()

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s8,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario3,
            created=date(2021, 11, 29).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s8,
            description = 'Contenido',
            worked_hours = 3,
            user=usuario3,
            created=date(2021, 12, 1).strftime('%Y-%m-%d'),
        )

        user_s9 = UserStory.objects.create(
            project=proyecto1,
            name='Story 9',
            description='Y una prueba mas',
            priority=9,
            sprint=sprint1,
            estimate=7,
            member=miembro4,
            finish_date=date(2021, 11, 30).strftime('%Y-%m-%d'),
            status="DONE",
            worked_hours=7,
        )

        PastSprints.objects.create(
            sprint = sprint1,
            user_story = user_s9,
            story_status = user_s9.status,
            priority = user_s9.priority,
            member = user_s9.member,
            estimate = user_s9.estimate,
            worked_hours = user_s9.worked_hours,
        )

        user_s9.status = "RELEASE"
        user_s9.save()

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s9,
            description = 'Contenido',
            worked_hours = 4,
            user=usuario4,
            created=date(2021, 11, 25).strftime('%Y-%m-%d'),
        )

        activity = Activity.objects.create(
            sprint = sprint1,
            user_story = user_s9,
            description = 'Contenido',
            worked_hours = 3,
            user=usuario4,
            created=date(2021, 11, 30).strftime('%Y-%m-%d'),
        )

        EventoUserStory.objects.create(
            user_story=user_s, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s.name, user_s.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s2, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s2.name, user_s2.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s3, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s3.name, user_s3.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s4, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s4.name, user_s4.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s5, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s5.name, user_s5.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s6, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s6.name, user_s6.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s7, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s7.name, user_s7.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s8, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s8.name, user_s8.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s9, 
            user=usuario2, 
            description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s9.name, user_s9.description, 'NEW'),
            date=date(2021, 11, 17).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s.name, user_s.description, 'TODO'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s2, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s2.name, user_s2.description, 'TODO'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s3, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s3.name, user_s3.description, 'TODO'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s4, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s4.name, user_s4.description, 'TODO'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s5, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s5.name, user_s5.description, 'TODO'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s6, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s6.name, user_s6.description, 'TODO'),
            date=date(2021, 11, 25).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s7, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s7.name, user_s7.description, 'TODO'),
            date=date(2021, 11, 25).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s8, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s8.name, user_s8.description, 'TODO'),
            date=date(2021, 11, 25).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s9, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0} y se lo agrega al sprint backlog del proyecto con id = {1}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s9.name, user_s9.description, 'TODO'),
            date=date(2021, 11, 25).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s, 
            user=usuario1, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario1.email, 2, user_s.name, user_s.description, 'DOING'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s2, 
            user=usuario1, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario1.email, 2, user_s2.name, user_s2.description, 'DOING'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s3, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s3.name, user_s3.description, 'DOING'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s4, 
            user=usuario3, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario3.email, 2, user_s4.name, user_s4.description, 'DOING'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s5, 
            user=usuario4, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario4.email, 2, user_s5.name, user_s5.description, 'DOING'),
            date=date(2021, 11, 18).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s6, 
            user=usuario1, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario1.email, 2, user_s6.name, user_s6.description, 'DOING'),
            date=date(2021, 11, 25).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s7, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s7.name, user_s7.description, 'DOING'),
            date=date(2021, 11, 25).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s8, 
            user=usuario3, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario3.email, 2, user_s8.name, user_s8.description, 'DOING'),
            date=date(2021, 11, 25).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s9, 
            user=usuario4, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario4.email, 2, user_s9.name, user_s9.description, 'DOING'),
            date=date(2021, 11, 25).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s, 
            user=usuario1, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario1.email, 2, user_s.name, user_s.description, 'DONE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s2, 
            user=usuario1, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario1.email, 2, user_s2.name, user_s2.description, 'DONE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s3, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s3.name, user_s3.description, 'DONE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s4, 
            user=usuario3, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario3.email, 2, user_s4.name, user_s4.description, 'DONE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s5, 
            user=usuario4, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario4.email, 2, user_s5.name, user_s5.description, 'DONE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s6, 
            user=usuario1, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario1.email, 2, user_s6.name, user_s6.description, 'DONE'),
            date=date(2021, 12, 1).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s7, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s7.name, user_s7.description, 'DONE'),
            date=date(2021, 12, 1).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s8, 
            user=usuario3, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario3.email, 2, user_s8.name, user_s8.description, 'DONE'),
            date=date(2021, 12, 1).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s9, 
            user=usuario4, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario4.email, 2, user_s9.name, user_s9.description, 'DONE'),
            date=date(2021, 12, 1).strftime('%Y-%m-%d')
        )
        EventoUserStory.objects.create(
            user_story=user_s, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s.name, user_s.description, 'TESTING'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s2, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s2.name, user_s2.description, 'TESTING'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s3, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s3.name, user_s3.description, 'TESTING'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s4, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s4.name, user_s4.description, 'TESTING'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s5, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s5.name, user_s5.description, 'TESTING'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s6, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s6.name, user_s6.description, 'TESTING'),
            date=date(2021, 12, 1).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s7, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s7.name, user_s7.description, 'TESTING'),
            date=date(2021, 12, 1).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s8, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario3.email, 2, user_s8.name, user_s8.description, 'TESTING'),
            date=date(2021, 12, 1).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s9, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s9.name, user_s9.description, 'TESTING'),
            date=date(2021, 12, 1).strftime('%Y-%m-%d')
        )
        EventoUserStory.objects.create(
            user_story=user_s, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s.name, user_s.description, 'RELEASE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s2, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s2.name, user_s2.description, 'REJECTED'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s3, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s3.name, user_s3.description, 'RELEASE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s4, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s4.name, user_s4.description, 'REJECTED'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s5, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s5.name, user_s5.description, 'RELEASE'),
            date=date(2021, 11, 24).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s6, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s6.name, user_s6.description, 'RELEASE'),
            date=date(2021, 12, 1).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s7, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s7.name, user_s7.description, 'RELEASE'),
            date=date(2021, 12, 1).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s8, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario3.email, 2, user_s8.name, user_s8.description, 'RELEASE'),
            date=date(2021, 12, 1).strftime('%Y-%m-%d')
        )

        EventoUserStory.objects.create(
            user_story=user_s9, 
            user=usuario2, 
            description="Se modifica el user story por el usuario {0}, Nombre: {2}, Descripcion: {3}, Estado: {4}"
                        .format(usuario2.email, 2, user_s9.name, user_s9.description, 'RELEASE'),
            date=date(2021, 12, 1).strftime('%Y-%m-%d')
        )

        return Response(data={'ok':  True, 'msg': 'Base de datos poblada!' },status=status.HTTP_200_OK)