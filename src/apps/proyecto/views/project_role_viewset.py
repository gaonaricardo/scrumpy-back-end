from rest_framework import  status, views
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from proyecto.models.member import Member

from proyecto.models.project import Project
from proyecto.models.project_role import ProjectRole
from autenticacion.utils import getResponse

from proyecto.serializers import  (
    CreateProjectRoleSerializer, 
    ListOrRetrieveProjectRoleSerializer, 
    UpdateProjectRoleSerializer,
    )

"""
 * @apiGroup Roles de proyecto
 * @api {post} /projectModule/projects/:id_project/roles/ Crear un rol de proyecto 
 * @apiDescription Agrega un rol de proyecto
 * @apiParam {Number} id_project id del proyecto
 * @apiBody {Number[]} id_permissions Lista de los id de los permisos asignados al rol
 * @apiBody {String} name nombre del projectRol
 * @apiBody {String} description descripción del projectRol
 * @apiExample {json} Example body:
{
    "permissions": [
        1
    ],
    "name": "Nombre del rol",
    "description": ""
}
 * @apiSuccessExample {json} Success-Response:
    HTTP/1.1 201 OK
{
    "id": 6,
    "project": {
        "id": 1,
        "scrum_master": {
            "id": 1,
            "email": "mateogiulliano@hotmail.com",
            "first_name": "Giulliano",
            "last_name": "Albrecht",
            "address": "",
            "phone_number": "",
            "system_role": 1,
            "is_active": true
        },
        "name": "Proyecto de Prueba",
        "description": "",
        "status": "INICIADO",
        "startDay": "2021-09-01",
        "endDay": "2021-09-14"
    },
    "permissions": [
        1
    ],
    "name": "Developer",
    "description": ""
}
"""

"""
 * @apiGroup Roles de proyecto
 * @api {get} /projectModule/projects/:id_project/roles/ Recuperar los roles de proyecto
 * @apiDescription Recupera todos los roles de proyecto
 * @apiParam {Number} id_project id del proyecto
 * @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
[
    {
        "id": 1,
        "project": {
            "id": 1,
            "scrum_master": {
                "id": 1,
                "email": "mateogiulliano@hotmail.com",
                "first_name": "Giulliano",
                "last_name": "Albrecht",
                "address": "",
                "phone_number": "",
                "system_role": 1,
                "is_active": true
            },
            "name": "Proyecto de Prueba",
            "description": "",
            "status": "INICIADO",
            "startDay": "2021-09-01",
            "endDay": "2021-09-14"
        },
        "permissions": [
            1
        ],
        "name": "Developer",
        "description": ""
    },
    {
        "id": 2,
        "project": {
            "id": 1,
            "scrum_master": {
                "id": 1,
                "email": "mateogiulliano@hotmail.com",
                "first_name": "Giulliano",
                "last_name": "Albrecht",
                "address": "",
                "phone_number": "",
                "system_role": 1,
                "is_active": true
            },
            "name": "Proyecto de Prueba",
            "description": "",
            "status": "INICIADO",
            "startDay": "2021-09-01",
            "endDay": "2021-09-14"
        },
        "permissions": [
            1
        ],
        "name": "Developer",
        "description": ""
    },
    {
        "id": 3,
        "project": {
            "id": 1,
            "scrum_master": {
                "id": 1,
                "email": "mateogiulliano@hotmail.com",
                "first_name": "Giulliano",
                "last_name": "Albrecht",
                "address": "",
                "phone_number": "",
                "system_role": 1,
                "is_active": true
            },
            "name": "Proyecto de Prueba",
            "description": "",
            "status": "INICIADO",
            "startDay": "2021-09-01",
            "endDay": "2021-09-14"
        },
        "permissions": [
            1
        ],
        "name": "Developer",
        "description": ""
    },
    {
        "id": 4,
        "project": {
            "id": 1,
            "scrum_master": {
                "id": 1,
                "email": "mateogiulliano@hotmail.com",
                "first_name": "Giulliano",
                "last_name": "Albrecht",
                "address": "",
                "phone_number": "",
                "system_role": 1,
                "is_active": true
            },
            "name": "Proyecto de Prueba",
            "description": "",
            "status": "INICIADO",
            "startDay": "2021-09-01",
            "endDay": "2021-09-14"
        },
        "permissions": [
            1
        ],
        "name": "Developer",
        "description": ""
    },
    {
        "id": 5,
        "project": {
            "id": 1,
            "scrum_master": {
                "id": 1,
                "email": "mateogiulliano@hotmail.com",
                "first_name": "Giulliano",
                "last_name": "Albrecht",
                "address": "",
                "phone_number": "",
                "system_role": 1,
                "is_active": true
            },
            "name": "Proyecto de Prueba",
            "description": "",
            "status": "INICIADO",
            "startDay": "2021-09-01",
            "endDay": "2021-09-14"
        },
        "permissions": [
            1
        ],
        "name": "Developer",
        "description": ""
    },
    {
        "id": 6,
        "project": {
            "id": 1,
            "scrum_master": {
                "id": 1,
                "email": "mateogiulliano@hotmail.com",
                "first_name": "Giulliano",
                "last_name": "Albrecht",
                "address": "",
                "phone_number": "",
                "system_role": 1,
                "is_active": true
            },
            "name": "Proyecto de Prueba",
            "description": "",
            "status": "INICIADO",
            "startDay": "2021-09-01",
            "endDay": "2021-09-14"
        },
        "permissions": [
            1
        ],
        "name": "Developer",
        "description": ""
    }
]
"""

"""
 * @apiGroup Roles de proyecto
 * @api {get} /projectModule/projects/:id_project/roles/:id_role/ Recuperar un rol de proyecto  de un proyecto
 * @apiDescription Recupera un rol de proyecto
 * @apiParam {Number} id_project id del proyecto
 * @apiParam {Number} id_role id del rol a nivel de proyecto
 * @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
{
    "id": 1,
    "project": {
        "id": 1,
        "scrum_master": {
            "id": 1,
            "email": "mateogiulliano@hotmail.com",
            "first_name": "Giulliano",
            "last_name": "Albrecht",
            "address": "",
            "phone_number": "",
            "system_role": 1,
            "is_active": true
        },
        "name": "Proyecto de Prueba",
        "description": "",
        "status": "INICIADO",
        "startDay": "2021-09-01",
        "endDay": "2021-09-14"
    },
    "permissions": [
        1
    ],
    "name": "Developer",
    "description": ""
}
"""

"""
 * @apiGroup Roles de proyecto
 * @api {put} /projectModule/projects/:id_project/roles/:id_role/ Actualizar un rol de proyecto
 * @apiDescription Actualiza un rol de proyecto
 * @apiParam {Number} id_project id del proyecto
 * @apiParam {Number} id_role id del rol
 * @apiBody {Number[]} id_permissions Lista de los id de los permisos asignados al rol
 * @apiBody {String} name nombre del projectRol
 * @apiBody {String} description descripción del projectRol
 * @apiExample {json} Example body:
{
    "permissions": [
        1
    ],
    "name": "Developer",
    "description": ""
}
 * @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK - Devuelve el rol de proyecto actualizado
{
    "id": 1,
    "project": {
        "id": 1,
        "scrum_master": {
            "id": 1,
            "email": "mateogiulliano@hotmail.com",
            "first_name": "Giulliano",
            "last_name": "Albrecht",
            "address": "",
            "phone_number": "",
            "system_role": 1,
            "is_active": true
        },
        "name": "Proyecto de Prueba",
        "description": "",
        "status": "INICIADO",
        "startDay": "2021-09-01",
        "endDay": "2021-09-14"
    },
    "permissions": [
        1
    ],
    "name": "Developer",
    "description": ""
}
"""

"""
 * @apiGroup Roles de proyecto
 * @api {patch} /projectModule/projects/:id_project/roles/:id_role/ Actualizar un rol de proyecto
 * @apiDescription Actualiza parcialmente un rol de proyecto
 * @apiParam {Number} id_project id del proyecto
 * @apiParam {Number} id_role id del rol
 * @apiBody {Number[]} id_permissions Lista de los id de los permisos asignados al rol
 * @apiBody {String} name nombre del projectRol
 * @apiBody {String} description descripción del projectRol
 * @apiExample {json} Example body:
{
    "permissions": [
        1
    ],
    "name": "Developer",
    "description": ""
}
 * @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK - Devuelve el rol de proyecto actualizado
{
    "id": 1,
    "project": {
        "id": 1,
        "scrum_master": {
            "id": 1,
            "email": "mateogiulliano@hotmail.com",
            "first_name": "Giulliano",
            "last_name": "Albrecht",
            "address": "",
            "phone_number": "",
            "system_role": 1,
            "is_active": true
        },
        "name": "Proyecto de Prueba",
        "description": "",
        "status": "INICIADO",
        "startDay": "2021-09-01",
        "endDay": "2021-09-14"
    },
    "permissions": [
        1
    ],
    "name": "Developer",
    "description": ""
}
"""

"""
 * @apiGroup Roles de proyecto
 * @api {delete} /projectModule/projects/:id_project/roles/:id_role/ Eliminar un rol de proyecto
 * @apiDescription Elimina un rol de proyecto siempre que un miembro no esté relacionado con el rol de proyecto
 * @apiParam {Number} id_project id del proyecto
 * @apiParam {Number} id_role id del rol
 * @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK - None
"""

class ProjectRoleView(views.APIView):
    def post(self, req, id_project):
        serializer = CreateProjectRoleSerializer(data=req.data)
        if serializer.is_valid():
            projectRole = serializer.save()
            project = Project.objects.get(pk=id_project)
            projectRole.project = project
            projectRole.save()
            new_serializer = ListOrRetrieveProjectRoleSerializer(projectRole)
            return Response(data=new_serializer.data, status=status.HTTP_201_CREATED)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def get(self, req, id_project):
        projectRoles = ProjectRole.objects.filter(project=id_project)
        serializer = ListOrRetrieveProjectRoleSerializer(projectRoles,many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class ProjectRoleRetrieveView(views.APIView):
    def get(self, req, id_project, id_role):
        projectRole = get_object_or_404(ProjectRole, pk=id_role)
        serializer = ListOrRetrieveProjectRoleSerializer(projectRole)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def put(self, req, id_project, id_role):
        serializer = UpdateProjectRoleSerializer(data=req.data)
        projectRole = get_object_or_404(ProjectRole, pk=id_role)
        if serializer.is_valid():
            updated_projectRole = serializer.update(projectRole, serializer.validated_data)
            updated_serializer = ListOrRetrieveProjectRoleSerializer(updated_projectRole)
            return Response(data=updated_serializer.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def patch(self, req, id_project, id_role):
        serializer = UpdateProjectRoleSerializer(data=req.data, partial=True)
        projectRole = get_object_or_404(ProjectRole, pk=id_role)
        if serializer.is_valid():
            updated_projectRole = serializer.update(projectRole, serializer.validated_data)
            updated_serializer = ListOrRetrieveProjectRoleSerializer(updated_projectRole)
            return Response(data=updated_serializer.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self, req, id_project, id_role):

        members = Member.objects.filter(project=id_project)

        for member in members:
            if member.projectRole.id == id_role:
                print("entre")
                data = {'code': 4, 'message': 'El Rol Proyecto esta en uso'}
                return Response(getResponse(False, data), status=status.HTTP_400_BAD_REQUEST)               
        
        projectRole = get_object_or_404(ProjectRole, pk=id_role)
        aux = ListOrRetrieveProjectRoleSerializer(projectRole)
        projectRole.delete()
        return Response(data=aux.data, status=status.HTTP_200_OK)
