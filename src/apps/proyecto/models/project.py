from proyecto.models.project_status import ProjectStatus
from django.db import models
from autenticacion.models import CustomUser

"""
    Modelo Project

    Campos: id, name, description, scrum_master
"""

class Project(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField(default="")
    scrum_master = models.ForeignKey(CustomUser, on_delete=models.PROTECT, null=True)
    status = models.CharField(
        max_length=10,
        choices=ProjectStatus.choices,
        default=ProjectStatus.NEW,
    )
    startDay = models.DateField(null=True, blank=True)
    endDay = models.DateField(null=True, blank=True)
    sprintDuration = models.IntegerField(null=True, default=1, blank=True)
    
    def __str__(self):
        return self.name