from django.db import models
from .project_permission import ProjectPermission
from proyecto.models import Project

"""
    Modelo Rol proyecto

    Campos: id, nombre, descripcion y permisos[]
"""

class ProjectRole(models.Model):
    project = models.ForeignKey(Project, null=True, on_delete=models.CASCADE)
    permissions = models.ManyToManyField(ProjectPermission)
    name = models.CharField(max_length=50)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name