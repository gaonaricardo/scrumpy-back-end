from django.db import models


"""
    Modelo Permiso de proyecto

    Campos: id, nombre, descripcion
"""
class ProjectPermission(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    code = models.IntegerField()    

    def __str__(self):
        return self.name