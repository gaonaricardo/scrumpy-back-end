# Django
from django.test import TestCase

# Python
import json

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status
from proyecto.models import ProjectRole,ProjectPermission, Project

# Models
from proyecto.models import Member, Project
from autenticacion.models import CustomUser


class MemberTestCase(TestCase):

    def test_create_member(self):

        client = APIClient()

        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=usuario)

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = usuario  
        )
        
        permiso = ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )
        
        role = ProjectRole.objects.create(
            name = 'RolProyecto_test',
            description ='Este es el RolProyecto',
            project = proyecto,
        )
        role.permissions.add(permiso)

        test_member = {
            'user' : usuario.email,
            "projectRole": role.id,
            "availability": 10.0,
            "is_active": True,
        }

        response = client.post(
            f'/api/projectModule/projects/{proyecto.id}/members/', 
            test_member,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', result)
        self.assertIn('user', result)
        self.assertIn('project', result)

        if 'id' in result:
            del result['id']

    def test_update_member(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=usuario)

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = usuario 
        )
        
        miembro = Member.objects.create(
            user = usuario,
            project = proyecto,
        )

        permiso = ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )
        
        role = ProjectRole.objects.create(
            name = 'RolProyecto_test',
            description ='Este es el RolProyecto',
            project = proyecto,
        )
        role.permissions.add(permiso)

        test_member_update = {
            'user' : usuario.id,
            "projectRole": role.id,
            "availability": 10.0
        }

        response = client.put(
            f'/api/projectModule/projects/{proyecto.id}/members/{miembro.id}/', 
            test_member_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        if 'id' in result:
            del result['id']

    def test_delete_member(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=usuario)

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = usuario  
        )
        
        sprint = Member.objects.create(
            user = usuario,
            project = proyecto,
        )


        response = client.delete(
            f'/api/projectModule/projects/{proyecto.id}/members/{sprint.id}/', 
            format='json'
        )

        sprint_exists = Member.objects.filter(id=sprint.id)
        self.assertFalse(sprint_exists)


    def test_get_member(self):

        client = APIClient()

        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=usuario)

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = usuario  
        )
        
        sprint1 = Member.objects.create(
            user= usuario,
            project= proyecto,
        )


        sprint2 = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        

        response = client.get(f'/api/projectModule/projects/{proyecto.id}/members/')
        
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


        for rol in result:
            self.assertIn('id', rol)
            self.assertIn('user', rol)
            self.assertIn('project', rol)
            break

    def test_create_member_unauthorized(self):

        client = APIClient()

        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = usuario  
        )
        
        permiso = ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )
        
        role = ProjectRole.objects.create(
            name = 'RolProyecto_test',
            description ='Este es el RolProyecto',
            project = proyecto,
        )
        role.permissions.add(permiso)

        test_member = {
            'user' : usuario.email,
            "projectRole": role.id,
            "availability": 10.0,
            "is_active": True,
        }

        response = client.post(
            f'/api/projectModule/projects/{proyecto.id}/members/', 
            test_member,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_member_unauthorized(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = usuario 
        )
        
        miembro = Member.objects.create(
            user = usuario,
            project = proyecto,
        )

        permiso = ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )
        
        role = ProjectRole.objects.create(
            name = 'RolProyecto_test',
            description ='Este es el RolProyecto',
            project = proyecto,
        )
        role.permissions.add(permiso)

        test_member_update = {
            'user' : usuario.id,
            "projectRole": role.id,
            "availability": 10.0
        }

        response = client.put(
            f'/api/projectModule/projects/{proyecto.id}/members/{miembro.id}/', 
            test_member_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_member_unauthorized(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = usuario  
        )
        
        sprint = Member.objects.create(
            user = usuario,
            project = proyecto,
        )


        response = client.delete(
            f'/api/projectModule/projects/{proyecto.id}/members/{sprint.id}/', 
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


    def test_get_member_unauthorized(self):

        client = APIClient()

        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = usuario  
        )
        
        sprint1 = Member.objects.create(
            user= usuario,
            project= proyecto,
        )


        sprint2 = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        

        response = client.get(f'/api/projectModule/projects/{proyecto.id}/members/')
        
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)