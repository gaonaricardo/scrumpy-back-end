# Django
from django.test import TestCase

# Python
import json

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status
from autenticacion.models import CustomUser

# Models
from proyecto.models import Project, Member


class ProjectTestCase(TestCase):

    def test_create_project(self):

        client = APIClient()

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        test_project = {
            'name' :'Proyecto_test',
            'description' :'Este es el Proyecto',
            'scrum_master' : user.email,
            'startDay': '2021-10-03',
            'endDay': '2022-02-17'
        }

        response = client.post(
            '/api/projectModule/projects/', 
            test_project,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', result)
        self.assertIn('name', result)
        self.assertIn('description', result)
        self.assertIn('scrum_master', result)

        if 'id' in result:
            del result['id']

    def test_update_project(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con dato

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        project = Project.objects.create(
            name = 'Proyecto_test',
            description ='Este es el Proyecto',
            scrum_master = user
        )

        test_project_update = {
            'name' :'Proyecto_test_updated'
        }

        response = client.patch(
            f'/api/projectModule/projects/{project.id}/', 
            test_project_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        if 'id' in result:
            del result['id']

    def test_get_project(self):

        client = APIClient()
        
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        project1 = Project.objects.create(
            name = 'Proyecto_test',
            description ='Este es el Proyecto',
            scrum_master = user
        )

        project2 = Project.objects.create(
            name = 'Proyecto_test',
            description ='Este es el Proyecto',
            scrum_master = user
        )       

        response = client.get('/api/projectModule/projects/')
        
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_project_unauthorized(self):

        client = APIClient()

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        test_project = {
            'name' :'Proyecto_test',
            'description' :'Este es el Proyecto',
            'scrum_master' : user.email,
            'startDay': '2021-10-03',
            'endDay': '2022-02-17'
        }

        response = client.post(
            '/api/projectModule/projects/', 
            test_project,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_project_unauthorized(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con dato

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        project = Project.objects.create(
            name = 'Proyecto_test',
            description ='Este es el Proyecto',
            scrum_master = user
        )

        test_project_update = {
            'name' :'Proyecto_test_updated'
        }

        response = client.patch(
            f'/api/projectModule/projects/{project.id}/', 
            test_project_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_project_unauthorized(self):

        client = APIClient()
        
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        project1 = Project.objects.create(
            name = 'Proyecto_test',
            description ='Este es el Proyecto',
            scrum_master = user
        )

        project2 = Project.objects.create(
            name = 'Proyecto_test',
            description ='Este es el Proyecto',
            scrum_master = user
        )       

        response = client.get('/api/projectModule/projects/')
        
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    