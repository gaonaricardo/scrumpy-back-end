# Django
from django.test import TestCase

# Python
import json

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status

# Models
from proyecto.models import ProjectRole,ProjectPermission, Project
from autenticacion.models import CustomUser


class ProjectRoleTestCase(TestCase):

    def test_create_project_role(self):

        client = APIClient()
        
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = user  
        )
        
        permiso = ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )

        test_project_role = {
            'name' :'RolProyecto_test',
            'description' :'Este es el RolProyecto',
            'permissions' : [permiso.id],
            'project' : proyecto.id
        }

        response = client.post(
            f'/api/projectModule/projects/{proyecto.id}/roles/', 
            test_project_role,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', result)
        self.assertIn('name', result)
        self.assertIn('description', result)
        self.assertIn('permissions', result)

        if 'id' in result:
            del result['id']

    def test_update_project_role(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con dato
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = user  
        )
        
        permiso = ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )
        
        role = ProjectRole.objects.create(
            name = 'RolProyecto_test',
            description ='Este es el RolProyecto',
            project = proyecto,
        )
        role.permissions.add(permiso)

        test_project_role_update = {
            'name' :'RolProyecto_test_updated',
            'description' :'Este es el RolProyecto Actualizado',
            'permissions' : [permiso.id],
            'project' : proyecto.id
        }

        response = client.put(
            f'/api/projectModule/projects/{proyecto.id}/roles/{role.id}/', 
            test_project_role_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        if 'id' in result:
            del result['id']

    def test_delete_project_role(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con dato
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = user  
        )
        
        permiso = ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )
        
        role = ProjectRole.objects.create(
            name = 'RolProyecto_test',
            description ='Este es el RolProyecto',
            project = proyecto,
        )

        role.permissions.add(permiso)

        response = client.delete(
            f'/api/projectModule/projects/{proyecto.id}/roles/{role.id}/', 
            format='json'
        )

        role_exists = ProjectRole.objects.filter(id=role.id)
        self.assertFalse(role_exists)


    def test_get_project_role(self):

        client = APIClient()
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = user  
        )
        
        permiso = ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )
        
        role1 = ProjectRole.objects.create(
            name = 'RolProyecto_test1',
            description ='Este es el RolProyecto1',
            project = proyecto,
        )
        role1.permissions.add(permiso)

        role2 = ProjectRole.objects.create(
            name = 'RolProyecto_test2',
            description ='Este es el RolProyecto2',
            project = proyecto,
        )
        role2.permissions.add(permiso)
        

        response = client.get(f'/api/projectModule/projects/{proyecto.id}/roles/')
        
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


        for rol in result:
            self.assertIn('id', rol)
            self.assertIn('name', rol)
            self.assertIn('description', rol)
            self.assertIn('permissions', rol)
            break

    def test_create_project_role_unauthorized(self):

        client = APIClient()
        
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = user  
        )
        
        permiso = ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )

        test_project_role = {
            'name' :'RolProyecto_test',
            'description' :'Este es el RolProyecto',
            'permissions' : [permiso.id],
            'project' : proyecto.id
        }

        response = client.post(
            f'/api/projectModule/projects/{proyecto.id}/roles/', 
            test_project_role,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_project_role_unauthorized(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con dato
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = user  
        )
        
        permiso = ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )
        
        role = ProjectRole.objects.create(
            name = 'RolProyecto_test',
            description ='Este es el RolProyecto',
            project = proyecto,
        )
        role.permissions.add(permiso)

        test_project_role_update = {
            'name' :'RolProyecto_test_updated',
            'description' :'Este es el RolProyecto Actualizado',
            'permissions' : [permiso.id],
            'project' : proyecto.id
        }

        response = client.put(
            f'/api/projectModule/projects/{proyecto.id}/roles/{role.id}/', 
            test_project_role_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_project_role_unauthorized(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con dato
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = user  
        )
        
        permiso = ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )
        
        role = ProjectRole.objects.create(
            name = 'RolProyecto_test',
            description ='Este es el RolProyecto',
            project = proyecto,
        )

        role.permissions.add(permiso)

        response = client.delete(
            f'/api/projectModule/projects/{proyecto.id}/roles/{role.id}/', 
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


    def test_get_project_role_unauthorized(self):

        client = APIClient()
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = user  
        )
        
        permiso = ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )
        
        role1 = ProjectRole.objects.create(
            name = 'RolProyecto_test1',
            description ='Este es el RolProyecto1',
            project = proyecto,
        )
        role1.permissions.add(permiso)

        role2 = ProjectRole.objects.create(
            name = 'RolProyecto_test2',
            description ='Este es el RolProyecto2',
            project = proyecto,
        )
        role2.permissions.add(permiso)
        

        response = client.get(f'/api/projectModule/projects/{proyecto.id}/roles/')
        
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)