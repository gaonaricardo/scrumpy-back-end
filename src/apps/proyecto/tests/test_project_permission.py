# Django
from django.test import TestCase

# Python
import json

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status
from autenticacion.models.user import CustomUser

# Models
from proyecto.models import ProjectPermission

class ProjectPermissionTestCase(TestCase):

    def test_get_projectPermision(self):

            client = APIClient()
        
            usuario = CustomUser.objects.create(
                email='ramako72@gmail.com',
                first_name='Raul',
                last_name='Galvan'
            )

            client.force_authenticate(user=usuario)

            ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )

            ProjectPermission.objects.create(
                name = 'Permiso2_test',
                description = 'Este es el permiso 2',
                code = 2
            )

            response = client.get('/api/projectModule/permission/')
            
            result = json.loads(response.content)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            for perm in result:
                self.assertIn('id', perm)
                self.assertIn('description', perm)
                self.assertIn('code', perm)
                break
    
    def test_get_projectPermision_unauthorized(self):

            client = APIClient()
        
            usuario = CustomUser.objects.create(
                email='ramako72@gmail.com',
                first_name='Raul',
                last_name='Galvan'
            )

            ProjectPermission.objects.create(
                name = 'Permiso1_test',
                description = 'Este es el permiso 1',
                code = 1  
            )

            ProjectPermission.objects.create(
                name = 'Permiso2_test',
                description = 'Este es el permiso 2',
                code = 2
            )

            response = client.get('/api/projectModule/permission/')
            
            result = json.loads(response.content)
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)