from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin
from autenticacion.models import SystemPermission
from autenticacion.serializers import SystemPermissionSerializer

"""
@apiGroup Permisos de sistema
@api {get} /permisos/ Traer todos los Permisos Disponible para el Usuario.
@apiSuccess {String} firstname Firstname of the User.
@apiSuccess {String} lastname  Lastname of the User.
"""

"""
@apiGroup Permisos de sistema
@api {get} /permisos/:id Request User information
@apiParam {Number} id Id del Permiso.
@apiSuccess {String} firstname Firstname of the User.
@apiSuccess {String} lastname  Lastname of the User.
"""

"""
@apiGroup Permisos de sistema
@api {post} /permisos/ Crear
@apiParam {Number} id Users unique ID.
@apiSuccess {String} firstname Firstname of the User.
@apiSuccess {String} lastname  Lastname of the User.
"""

class SystemPermissionViewset(GenericViewSet, ListModelMixin):
    queryset = SystemPermission.objects.all()
    serializer_class = SystemPermissionSerializer
