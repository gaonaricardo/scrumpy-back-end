from rest_framework import  status
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from autenticacion.models.system_role import SystemRole
from autenticacion.serializers import SystemRoleSerializer


"""
@apiGroup Roles de sistema
@api {get} /permisos/ Traer todos los Permisos Disponible para el Usuario.
@apiSuccess {String} firstname Firstname of the User.
@apiSuccess {String} lastname  Lastname of the User.
"""

"""
@apiGroup Roles de sistema
@api {get} /permisos/:id Request User information
@apiParam {Number} id Id del Permiso.
@apiSuccess {String} firstname Firstname of the User.
@apiSuccess {String} lastname  Lastname of the User.
"""

"""
@apiGroup Roles de sistema
@api {post} /permisos/ Crear
@apiParam {Number} id Users unique ID.
@apiSuccess {String} firstname Firstname of the User.
@apiSuccess {String} lastname  Lastname of the User.
"""

from autenticacion.models import CustomUser, SystemRole
from autenticacion.utils import getResponse

class SystemRoleViewset(ModelViewSet):
    queryset = SystemRole.objects.all()
    serializer_class = SystemRoleSerializer

    def destroy(self, req, pk):
        users = CustomUser.objects.all()

        for user in users:
            if user.system_role.id == int(pk):
                data = {'code': 3, 'message': 'El Rol esta en uso'}
                return Response(getResponse(False, data), status=status.HTTP_400_BAD_REQUEST)               
        systemRole = get_object_or_404(SystemRole, pk=pk)
        aux = SystemRoleSerializer(systemRole)
        systemRole.delete()
        return Response(data=aux.data, status=status.HTTP_200_OK)