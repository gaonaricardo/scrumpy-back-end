from autenticacion.serializers import CustomUserSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class ProfileView(APIView):
    def get(self, request):
        user = request.user
        user_serializer = CustomUserSerializer(user)
        return Response(data=user_serializer.data, status=status.HTTP_200_OK)