from rest_framework import views, status
from rest_framework.response import Response
from sprint.serializers import ListOrRetrieveUserStorySerializer

class UserTasksView(views.APIView):
    def get(self, req):
        # Obtenemos el usuario que hizo la petición
        current_user = req.user

        # Obtenemos todos los miembros que hagan referencia al current user
        miembros = current_user.member_set.all()

        # Agregamos en una lista todos los US asignados a los miembros
        stories = []
        for member in miembros:
            member_stories = member.userstory_set.all();
            for story in member_stories:
                stories.append(story)

        serializer = ListOrRetrieveUserStorySerializer(stories, many=True)
        
        return Response(data=serializer.data, status=status.HTTP_200_OK)
