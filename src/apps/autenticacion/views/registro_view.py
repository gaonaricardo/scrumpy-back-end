from rest_framework.viewsets import ModelViewSet
from autenticacion.serializers import CustomUserSerializer
from autenticacion.models import CustomUser

"""
@apiGroup Usuario
@api {get} /permisos/ Traer todos los Permisos Disponible para el Usuario.
@apiSuccess {String} firstname Firstname of the User.
@apiSuccess {String} lastname  Lastname of the User.
"""

"""
@apiGroup Usuario
@api {get} /permisos/:id Request User information
@apiParam {Number} id Id del Permiso.
@apiSuccess {String} firstname Firstname of the User.
@apiSuccess {String} lastname  Lastname of the User.
"""

"""
@apiGroup Usuario
@api {post} /permisos/ Crear
@apiParam {Number} id Users unique ID.
@apiSuccess {String} firstname Firstname of the User.
@apiSuccess {String} lastname  Lastname of the User.
"""

class UserViewset(ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer