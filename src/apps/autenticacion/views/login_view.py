import os
from rest_framework.response import Response
from autenticacion.models import CustomUser
from rest_framework import views, status, permissions
from autenticacion.serializers import CustomUserSerializer, LoginSerializar
from google.oauth2 import id_token
from google.auth.transport import requests
from rest_framework_jwt.utils import jwt_payload_handler, jwt_encode_handler
from autenticacion.utils import getResponse

class LoginView(views.APIView):
    permission_classes = [permissions.AllowAny,]
    def post(self, req):
        serializer = LoginSerializar(data=req.data)
        if serializer.is_valid(raise_exception=True):
            
            #Obtener el Id token del body
            token = serializer.validated_data.get('id_token')
            
            # Validar que sea de google 
            try:
                
                idinfo = id_token.verify_oauth2_token(token, requests.Request(), os.environ['CLIENT_ID'])

                print(idinfo)

                result = CustomUser.objects.filter(email=idinfo['email'])
                if(len(result) == 0):
                    data = {'code': 1, 'message': 'No existe un usuario con el correo especificado, por favor contacte a su project manager'}
                    return Response(getResponse(False, data), status=status.HTTP_400_BAD_REQUEST)
                user = result[0]
                if user.is_active == False:
                    data = {'code': 2, 'message': 'Tu usuario está desactivado, contacta con el administrador del sistema'}
                    return Response(getResponse(False, data), status=status.HTTP_403_FORBIDDEN)
                if user.google_id == '':
                    user.google_id = idinfo['sub']
                    user.save()
                user.picture = idinfo['picture']
                user.save()
                payload = jwt_payload_handler(user)
                encode = jwt_encode_handler(payload=payload)
                user_serializer = CustomUserSerializer(user)
                data = {'token': encode, 'usuario': user_serializer.data}
                
            except ValueError:
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                
            return Response(getResponse(True, data), status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)