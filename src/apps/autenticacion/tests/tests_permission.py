from django.test import TestCase

from rest_framework.test import APIClient
from rest_framework import status
from autenticacion.models.user import CustomUser


from autenticacion.models import SystemPermission
import json

class SystemPermissionTestCase(TestCase):

    def test_get_permissions(self):

        client = APIClient()
        
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=usuario)

        SystemPermission.objects.create(
            code=100,
            description="This is a dumb description"
        )

        response = client.get(
            '/api/permissions/',
            format='json'
        )

        result = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
        for permission in result:
            self.assertIn('code', permission)
            self.assertIn('name', permission)
            self.assertIn('description', permission)

    def test_get_permissions_unauthorized(self):

        client = APIClient()
        
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        SystemPermission.objects.create(
            code=100,
            description="This is a dumb description"
        )

        response = client.get(
            '/api/permissions/',
            format='json'
        )

        result = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        

