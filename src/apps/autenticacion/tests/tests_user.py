from django.http import response
from django.test import TestCase

from rest_framework.test import APIClient
from rest_framework import status

from autenticacion.models import CustomUser
import json

class UserTestCase(TestCase):

    def test_create_user(self):

        client = APIClient()
        
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=usuario)

        test_user = {
            'email':'mgalbretch20005@gmail.com',
            'first_name': 'Giulliano',
            'last_name': 'Albretch',
        }

        response = client.post(
            '/api/users/',
            test_user,
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_user(self):

        client = APIClient()
        
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=usuario)

        user = CustomUser.objects.create(
            email='mgalbretch20005@gmail.com',
            first_name='Giulliano',
            last_name='Albretch'
        )

        test_user = {
            'email': 'mgalbretch20005@gmail.com',
            'first_name': 'Mateo Giulliano',
            'last_name': 'Albretch Cuevas',
        }

        response = client.put(
            "/api/users/{0}/".format(user.id),
            test_user,
            format='json'
        )

        result = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('id', result)
        self.assertIn('email', result)
        self.assertIn('first_name', result)
        self.assertIn('last_name', result)

    def test_create_user_unauthorized(self):

        client = APIClient()
        
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        test_user = {
            'email':'mgalbretch20005@gmail.com',
            'first_name': 'Giulliano',
            'last_name': 'Albretch',
        }

        response = client.post(
            '/api/users/',
            test_user,
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_user_unauthorized(self):

        client = APIClient()
        
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        user = CustomUser.objects.create(
            email='mgalbretch20005@gmail.com',
            first_name='Giulliano',
            last_name='Albretch'
        )

        test_user = {
            'email': 'mgalbretch20005@gmail.com',
            'first_name': 'Mateo Giulliano',
            'last_name': 'Albretch Cuevas',
        }

        response = client.put(
            "/api/users/{0}/".format(user.id),
            test_user,
            format='json'
        )

        result = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)