from django.urls.conf import path, include
from .views import LoginView, ProfileView, SystemPermissionViewset, SystemRoleViewset, UserViewset, UserTasksView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register('permissions', SystemPermissionViewset, basename='permissions')
router.register('roles', SystemRoleViewset, basename='roles')
router.register('users', UserViewset, basename='users')

urlpatterns = [
    path('login/', LoginView.as_view()),
    path('profile/', ProfileView.as_view()),
    path('tasks/', UserTasksView.as_view()),
    path('', include(router.urls)),
]
