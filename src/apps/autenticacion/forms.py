"""
Formularios

1. **CustomUserCreationForm** - ([[forms.py#CustomUserCreationForm]])
2. **CustomUserChangeForm** - ([[forms.py#CustomUserChangeForm]])
"""

from django.contrib.auth.forms import UserCreationForm, UserChangeForm, ReadOnlyPasswordHashField
from django import forms

# CustomUser definido en [[models.py]]
from .models import CustomUser


# === CustomUserCreationForm ===
class CustomUserCreationForm(UserCreationForm):
    """
    Clase que se encarga de crear .....
    """
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password Confirmation', widget=forms.PasswordInput)

    class Meta:
        models=CustomUser
        fields = ('email', 'first_name', 'last_name', 'is_staff', 'is_superuser')

    # === clean_password2 ===
    def clean_password2(self):
        """
        Borrar contraseña
        """
        p1 = self.cleaned_data.get("password1")
        p2 = self.cleaned_data.get("password2")

        if p1 and p2 and p1 != p2:
            raise forms.ValidationError("Passwords don't match!")

        return p2
        
    # === CustomUserCreationForm ===
    def save(self, commit=True):
        """
        Guardar cambios
        """
        user = super().save(commit=False)
        #user.set_password(self.cleaned_data.get("password1"))
        user.set_unusable_password()
        if commit:
            user.save()
        return user

# === CustomUserChangeForm ===
class CustomUserChangeForm(UserChangeForm):
    """
    Clase que se encarga de crear .....
    """

    password = ReadOnlyPasswordHashField()

    class Meta:
        models=CustomUser
        fields = ('email', 'first_name', 'last_name', 'groups', 'is_staff', 'is_superuser')

    # === clean_password ===
    def clean_password(self):
        """
        Borrar contraseña
        """
        return self.initial["password"]
