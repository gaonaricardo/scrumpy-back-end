from django.db.models import fields
from .models import CustomUser
from autenticacion.models import SystemPermission, SystemRole
from rest_framework import serializers

class CustomUserSerializer(serializers.ModelSerializer):
    system_role = serializers.PrimaryKeyRelatedField(queryset=SystemRole.objects.all(), required=False)
    picture = serializers.CharField(required=False)
    class Meta:
        model = CustomUser
        fields = ['id', 'email', 'first_name', 'last_name', 'address', 'phone_number', 'system_role', 'is_active', 'picture']

class LoginSerializar(serializers.Serializer):
    id_token = serializers.CharField()

class SystemRoleSerializer(serializers.ModelSerializer):
    permissions = serializers.PrimaryKeyRelatedField(
        queryset=SystemPermission.objects.all(),
        many=True
    )
    class Meta:
        model = SystemRole
        fields = '__all__'

class SystemPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = SystemPermission
        fields = '__all__'
    