from django.db import models
from .system_permission import SystemPermission

class SystemRole(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(null=True, blank=True)
    permissions = models.ManyToManyField(SystemPermission)

    def __str__(self):
        return self.name
