from django.db import models

class SystemPermission(models.Model):
    code = models.IntegerField()
    name = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return self.name