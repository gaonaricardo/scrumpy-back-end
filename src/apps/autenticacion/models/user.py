"""
Modelos
"""
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from autenticacion.models import SystemRole
# === CustomUserManager ===
class CustomUserManager(BaseUserManager):
    """
    Clase que se encarga de manejar nuestro Usuario customizado
    """
    # === create_user ===
    def create_user(self, email, first_name, last_name, password=None):
        """
        Argumentos: Self hace referencia a la instancia actual de la clase, 
        en este caso un Usuario, Recibe el Email, Nombre de USuario, 
        Nro de telefono y Contraseña
        Retorna: Un usuario
        """
        if not email:
            raise ValueError("Users must have an email address")
        if not first_name:
            raise ValueError("Users must have a first name")
        if not last_name:
            raise ValueError("Users must have a last name")

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name
        )

        user.set_password(password)
        user.save(using=self._db)

        return user
        
    # === create_superuser ===
    def create_superuser(self, email, first_name, last_name, password=None):
        """
        Argumentos: Self hace referencia a la instancia actual de la clase, 
        en este caso un Usuario, Recibe el Email, Nombre de USuario, 
        Nro de telefono y Contraseña
        Retorna: Un Super Usuario
        """
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
            first_name=first_name,
            last_name=last_name
        )

        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True

        user.save(using=self._db)

        return user

# === CustomUser ===
class CustomUser(AbstractBaseUser, PermissionsMixin):
    """
    Se crea un Usuario Customizado
    """

    google_id       = models.CharField(max_length=256, default='')
    email           = models.EmailField(verbose_name="email", max_length=60, unique=True)
    first_name      = models.CharField(max_length=50, default="")
    last_name       = models.CharField(max_length=50, default="")
    address          = models.CharField(max_length=200, null=True, blank=True, default="")
    phone_number    = models.CharField(max_length=50, null=True, blank=True, default="")
    system_role     = models.ForeignKey(SystemRole, on_delete=models.SET_NULL, null=True)
    picture         = models.CharField(max_length=300, default="")
    date_joined     = models.DateTimeField(verbose_name="date joined", auto_now_add=True)
    last_login      = models.DateTimeField(verbose_name="last login", auto_now=True)
    is_admin        = models.BooleanField(default=False)
    is_active       = models.BooleanField(default=True)
    is_staff        = models.BooleanField(default=False)
    is_superuser    = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'            # users are gonna be identified by their email addresses
    REQUIRED_FIELDS = ['first_name', 'last_name',]

    objects = CustomUserManager()
    
    def __str__(self):
        return self.email
    
    # === has_perm ===
    def has_perm(self, perm, obj=None):
        """
        Verificamos si el usuario tiene permiso 
        """
        return self.is_admin


    def has_module_perms(self, app_label):
        
        return True

    # === get_full_name ===
    def get_full_name(self):
        """
        Para retornar el UserName del Usuario
        """
        return self.first_name + ' ' + self.last_name
