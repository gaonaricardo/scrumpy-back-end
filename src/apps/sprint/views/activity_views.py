from functools import partial
from rest_framework import views, status
from rest_framework.decorators import api_view
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from sprint.models import Activity
from sprint.models.sprint import Sprint
from sprint.serializers.activity_serializers import CreateActivitySerializer, ListOrRetrieveActivitySerializer, UpdateActivitySerializer
from sprint.models.user_story import UserStory
from sprint.serializers.comment_serializers import CreateCommentSerializer, ListOrRetrieveCommentSerializer, UpdateCommentSerializer
from sprint.models import Comment, EventoUserStory

class ActivityListview(views.APIView):
    """
    * @api {post} sprintModule/projects/<int:id_project>/user_stories/<int:id_us>/activities/ Crear
    * @apiDescription Postea una nueva actividad para el user story especificado como parámetro.
    * @apiGroup Activity
    * @apiParam {Number} id_us El id del user story sobre el cual se desea comentar.
    * @apiParam {Number} id_sprint El id del sprint al cual pertenece el user story.
    * @apiBody {Number} worked_hours Horas trabajadas en el US
    * @apiBody {String} description Descripcion de la actividad
    * @apiSuccess {Number} id Un identificador numérico de la actividad.
    * @apiSuccess {Object} sprint datos del sprint al cual pertenece la actividad.
    * @apiSuccess {Object} user_story  Los datos del user story al cual fue adjuntado la actividad.
    * @apiSuccess {String} description La descripción de la actividad hecha.
    * @apiSuccess {String} created La fecha en la que se posteó la actividad.
    * @apiExample {js} Example usage:
        {
            "description" : "descripcion de una actividad",
            "worked_hours" : 3
        }  
    * @apiSuccessExample {json} Success-Response:
           {
                "id": 10,
                "sprint": {
                    "id": 1,
                    "project": {
                        "id": 1,
                        "scrum_master": {
                            "id": 1,
                            "email": "ramako72@gmail.com",
                            "first_name": "Raul",
                            "last_name": "Galvan",
                            "address": "",
                            "phone_number": "",
                            "system_role": 1,
                            "is_active": true,
                            "picture": "https://lh3.googleusercontent.com/a-/AOh14GhLhayq3m3xuUxiOXFTdHjUVnHOxnFJ_iCuRfpP=s96-c"
                        },
                        "name": "proyectito",
                        "description": "dfd",
                        "status": "INICIADO",
                        "startDay": "2021-10-23",
                        "endDay": "2021-10-09",
                        "sprintDuration": 1
                    },
                    "sprint_goal": "Por defecto",
                    "status": "EN_PLANIFICACION",
                    "iteration": 1,
                    "startDay": null,
                    "endDay": null
                },
                "user_story": {
                    "id": 1,
                    "project": {
                        "id": 1,
                        "scrum_master": {
                            "id": 1,
                            "email": "ramako72@gmail.com",
                            "first_name": "Raul",
                            "last_name": "Galvan",
                            "address": "",
                            "phone_number": "",
                            "system_role": 1,
                            "is_active": true,
                            "picture": "https://lh3.googleusercontent.com/a-/AOh14GhLhayq3m3xuUxiOXFTdHjUVnHOxnFJ_iCuRfpP=s96-c"
                        },
                        "name": "proyectito",
                        "description": "dfd",
                        "status": "INICIADO",
                        "startDay": "2021-10-23",
                        "endDay": "2021-10-09",
                        "sprintDuration": 1
                    },
                    "sprint": {
                        "id": 1,
                        "project": {
                            "id": 1,
                            "scrum_master": {
                                "id": 1,
                                "email": "ramako72@gmail.com",
                                "first_name": "Raul",
                                "last_name": "Galvan",
                                "address": "",
                                "phone_number": "",
                                "system_role": 1,
                                "is_active": true,
                                "picture": "https://lh3.googleusercontent.com/a-/AOh14GhLhayq3m3xuUxiOXFTdHjUVnHOxnFJ_iCuRfpP=s96-c"
                            },
                            "name": "proyectito",
                            "description": "dfd",
                            "status": "INICIADO",
                            "startDay": "2021-10-23",
                            "endDay": "2021-10-09",
                            "sprintDuration": 1
                        },
                        "sprint_goal": "Por defecto",
                        "status": "EN_PLANIFICACION",
                        "iteration": 1,
                        "startDay": null,
                        "endDay": null
                    },
                    "name": "primer user story",
                    "description": "",
                    "priority": 3,
                    "estimate": 3,
                    "worked_hours": null,
                    "status": "NEW",
                    "member": 1
                },
                "worked_hours": 3.0,
                "description": "descripcion de una actividad",
                "created": "2021-10-28T02:05:20.317618Z"
            }
    """
    def post(self, req, id_project, id_us):
        serializer = CreateActivitySerializer(data=req.data)
        if serializer.is_valid():
            activity = serializer.save()
            user_story = UserStory.objects.get(pk=id_us)
            user=req.user
            activity.user_story = user_story
            activity.sprint = user_story.sprint
            activity.user=req.user
            #print(user_story.sprint.id)
            #sprint = Sprint.objects.get(pk=user_story.sprint)
            activity.save()

            # Se registra el evento de comentar user story
            EventoUserStory.objects.create(
                user_story=user_story, 
                user=req.user, 
                description="El usuario {0} agregó una actividad al US"
                            .format(req.user.email)
            )

            if user_story.worked_hours == None:
                user_story.worked_hours = 0
                
            user_story.worked_hours = user_story.worked_hours + activity.worked_hours

            user_story.save()

            new_serializer = ListOrRetrieveActivitySerializer(activity)
            return Response(data=new_serializer.data, status=status.HTTP_201_CREATED)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    """
    * @api {get} sprintModule/projects/<int:id_project>/user_stories/<int:id_us>/activities/ Listar
    * @apiDescription Recupera todas las actividades de un user story.
    * @apiGroup Activity
    * @apiParam {Number} id_us El id del user story cuyos comentarios se desea obten
    * @apiParam {Number} id_sprint El id del sprint al cual pertenece el user story.er.
    * @apiSuccess {Number} id Un identificador numérico de la actividad.
    * @apiSuccess {Object} sprint datos del sprint al cual pertenece la actividad.
    * @apiSuccess {Object} user_story  Los datos del user story al cual fue adjuntado la actividad.
    * @apiSuccess {String} description La descripción de la actividad hecha.
    * @apiSuccess {String} created La fecha en la que se posteó la actividad.
    * @apiSuccessExample {json} Success-Response:
            {
                "id": 10,
                "sprint": {
                    "id": 1,
                    "project": {
                        "id": 1,
                        "scrum_master": {
                            "id": 1,
                            "email": "ramako72@gmail.com",
                            "first_name": "Raul",
                            "last_name": "Galvan",
                            "address": "",
                            "phone_number": "",
                            "system_role": 1,
                            "is_active": true,
                            "picture": "https://lh3.googleusercontent.com/a-/AOh14GhLhayq3m3xuUxiOXFTdHjUVnHOxnFJ_iCuRfpP=s96-c"
                        },
                        "name": "proyectito",
                        "description": "dfd",
                        "status": "INICIADO",
                        "startDay": "2021-10-23",
                        "endDay": "2021-10-09",
                        "sprintDuration": 1
                    },
                    "sprint_goal": "Por defecto",
                    "status": "EN_PLANIFICACION",
                    "iteration": 1,
                    "startDay": null,
                    "endDay": null
                },
                "user_story": {
                    "id": 1,
                    "project": {
                        "id": 1,
                        "scrum_master": {
                            "id": 1,
                            "email": "ramako72@gmail.com",
                            "first_name": "Raul",
                            "last_name": "Galvan",
                            "address": "",
                            "phone_number": "",
                            "system_role": 1,
                            "is_active": true,
                            "picture": "https://lh3.googleusercontent.com/a-/AOh14GhLhayq3m3xuUxiOXFTdHjUVnHOxnFJ_iCuRfpP=s96-c"
                        },
                        "name": "proyectito",
                        "description": "dfd",
                        "status": "INICIADO",
                        "startDay": "2021-10-23",
                        "endDay": "2021-10-09",
                        "sprintDuration": 1
                    },
                    "sprint": {
                        "id": 1,
                        "project": {
                            "id": 1,
                            "scrum_master": {
                                "id": 1,
                                "email": "ramako72@gmail.com",
                                "first_name": "Raul",
                                "last_name": "Galvan",
                                "address": "",
                                "phone_number": "",
                                "system_role": 1,
                                "is_active": true,
                                "picture": "https://lh3.googleusercontent.com/a-/AOh14GhLhayq3m3xuUxiOXFTdHjUVnHOxnFJ_iCuRfpP=s96-c"
                            },
                            "name": "proyectito",
                            "description": "dfd",
                            "status": "INICIADO",
                            "startDay": "2021-10-23",
                            "endDay": "2021-10-09",
                            "sprintDuration": 1
                        },
                        "sprint_goal": "Por defecto",
                        "status": "EN_PLANIFICACION",
                        "iteration": 1,
                        "startDay": null,
                        "endDay": null
                    },
                    "name": "primer user story",
                    "description": "",
                    "priority": 3,
                    "estimate": 3,
                    "worked_hours": null,
                    "status": "NEW",
                    "member": 1
                },
                "worked_hours": 3.0,
                "description": "descripcion de la actividad 2",
                "created": "2021-10-28T02:05:20.317618Z"
            }
    """
    def get(self, req, id_project, id_us):
        activities = Activity.objects.filter(user_story=id_us)
        serializer = ListOrRetrieveActivitySerializer(activities, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class ActivityRetrieveView(views.APIView):
    """
    * @api {get} sprintModule/projects/<int:id_project>/user_stories/<int:id_us>/activities/<int:id_activity>/ Detalle
    * @apiDescription Recupera una actividad específica de un user story.
    * @apiGroup Activity
    * @apiParam {Number} id_us El id del user story cuyos comentarios se desea obten
    * @apiParam {Number} id_sprint El id del sprint al cual pertenece el user story.er.
    * @apiParam {Number} id_comment El id del comentario que se desea obtener.
    * @apiSuccess {Number} id Un identificador numérico de la actividad.
    * @apiSuccess {Object} sprint datos del sprint al cual pertenece la actividad.
    * @apiSuccess {Object} user_story  Los datos del user story al cual fue adjuntado la actividad.
    * @apiSuccess {String} description La descripción de la actividad hecha.
    * @apiSuccess {String} created La fecha en la que se posteó la actividad.
    * @apiSuccessExample {json} Success-Response:
            {
                "id": 10,
                "sprint": {
                    "id": 1,
                    "project": {
                        "id": 1,
                        "scrum_master": {
                            "id": 1,
                            "email": "ramako72@gmail.com",
                            "first_name": "Raul",
                            "last_name": "Galvan",
                            "address": "",
                            "phone_number": "",
                            "system_role": 1,
                            "is_active": true,
                            "picture": "https://lh3.googleusercontent.com/a-/AOh14GhLhayq3m3xuUxiOXFTdHjUVnHOxnFJ_iCuRfpP=s96-c"
                        },
                        "name": "proyectito",
                        "description": "dfd",
                        "status": "INICIADO",
                        "startDay": "2021-10-23",
                        "endDay": "2021-10-09",
                        "sprintDuration": 1
                    },
                    "sprint_goal": "Por defecto",
                    "status": "EN_PLANIFICACION",
                    "iteration": 1,
                    "startDay": null,
                    "endDay": null
                },
                "user_story": {
                    "id": 1,
                    "project": {
                        "id": 1,
                        "scrum_master": {
                            "id": 1,
                            "email": "ramako72@gmail.com",
                            "first_name": "Raul",
                            "last_name": "Galvan",
                            "address": "",
                            "phone_number": "",
                            "system_role": 1,
                            "is_active": true,
                            "picture": "https://lh3.googleusercontent.com/a-/AOh14GhLhayq3m3xuUxiOXFTdHjUVnHOxnFJ_iCuRfpP=s96-c"
                        },
                        "name": "proyectito",
                        "description": "dfd",
                        "status": "INICIADO",
                        "startDay": "2021-10-23",
                        "endDay": "2021-10-09",
                        "sprintDuration": 1
                    },
                    "sprint": {
                        "id": 1,
                        "project": {
                            "id": 1,
                            "scrum_master": {
                                "id": 1,
                                "email": "ramako72@gmail.com",
                                "first_name": "Raul",
                                "last_name": "Galvan",
                                "address": "",
                                "phone_number": "",
                                "system_role": 1,
                                "is_active": true,
                                "picture": "https://lh3.googleusercontent.com/a-/AOh14GhLhayq3m3xuUxiOXFTdHjUVnHOxnFJ_iCuRfpP=s96-c"
                            },
                            "name": "proyectito",
                            "description": "dfd",
                            "status": "INICIADO",
                            "startDay": "2021-10-23",
                            "endDay": "2021-10-09",
                            "sprintDuration": 1
                        },
                        "sprint_goal": "Por defecto",
                        "status": "EN_PLANIFICACION",
                        "iteration": 1,
                        "startDay": null,
                        "endDay": null
                    },
                    "name": "primer user story",
                    "description": "",
                    "priority": 3,
                    "estimate": 3,
                    "worked_hours": null,
                    "status": "NEW",
                    "member": 1
                },
                "worked_hours": 3.0,
                "description": "descripcion de una actividad 3",
                "created": "2021-10-28T02:05:20.317618Z"
            }
    """
    def get(self, req, id_project, id_us, id_activity):
        activity = get_object_or_404(Activity, pk=id_activity)
        serializer = ListOrRetrieveActivitySerializer(activity)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    """
    * @api {put} sprintModule/projects/<int:id_project>/user_stories/<int:id_us>/activities/<int:id_activity>/ Modificar
    * @apiDescription Permite editar el campo description y worked_hours de la actividad y devuelve la actividad editada.
    * @apiGroup Activity
    * @apiParam {Number} id_us El id del user story cuya actividad se desea modificar
    * @apiParam {Number} id_sprint El id del sprint al cual pertenece el user story.
    * @apiParam {Number} id_actividad El id de la actividad que se desea modificar.
    * @apiBody {String} description Descripcion de la actividad
    * @apiBody {Number} description Descripcion de la activid
    * @apiSuccess {Number} id Un identificador numérico de la actividad.
    * @apiSuccess {Object} sprint datos del sprint al cual pertenece la actividad.
    * @apiSuccess {Object} user_story  Los datos del user story al cual fue adjuntado la actividad.
    * @apiSuccess {String} description La descripción de la actividad hecha.
    * @apiSuccess {String} created La fecha en la que se posteó la actividad.
    * @apiSuccessExample {json} Success-Response:
            {
                "id": 10,
                "sprint": {
                    "id": 1,
                    "project": {
                        "id": 1,
                        "scrum_master": {
                            "id": 1,
                            "email": "ramako72@gmail.com",
                            "first_name": "Raul",
                            "last_name": "Galvan",
                            "address": "",
                            "phone_number": "",
                            "system_role": 1,
                            "is_active": true,
                            "picture": "https://lh3.googleusercontent.com/a-/AOh14GhLhayq3m3xuUxiOXFTdHjUVnHOxnFJ_iCuRfpP=s96-c"
                        },
                        "name": "proyectito",
                        "description": "dfd",
                        "status": "INICIADO",
                        "startDay": "2021-10-23",
                        "endDay": "2021-10-09",
                        "sprintDuration": 1
                    },
                    "sprint_goal": "Por defecto",
                    "status": "EN_PLANIFICACION",
                    "iteration": 1,
                    "startDay": null,
                    "endDay": null
                },
                "user_story": {
                    "id": 1,
                    "project": {
                        "id": 1,
                        "scrum_master": {
                            "id": 1,
                            "email": "ramako72@gmail.com",
                            "first_name": "Raul",
                            "last_name": "Galvan",
                            "address": "",
                            "phone_number": "",
                            "system_role": 1,
                            "is_active": true,
                            "picture": "https://lh3.googleusercontent.com/a-/AOh14GhLhayq3m3xuUxiOXFTdHjUVnHOxnFJ_iCuRfpP=s96-c"
                        },
                        "name": "proyectito",
                        "description": "dfd",
                        "status": "INICIADO",
                        "startDay": "2021-10-23",
                        "endDay": "2021-10-09",
                        "sprintDuration": 1
                    },
                    "sprint": {
                        "id": 1,
                        "project": {
                            "id": 1,
                            "scrum_master": {
                                "id": 1,
                                "email": "ramako72@gmail.com",
                                "first_name": "Raul",
                                "last_name": "Galvan",
                                "address": "",
                                "phone_number": "",
                                "system_role": 1,
                                "is_active": true,
                                "picture": "https://lh3.googleusercontent.com/a-/AOh14GhLhayq3m3xuUxiOXFTdHjUVnHOxnFJ_iCuRfpP=s96-c"
                            },
                            "name": "proyectito",
                            "description": "dfd",
                            "status": "INICIADO",
                            "startDay": "2021-10-23",
                            "endDay": "2021-10-09",
                            "sprintDuration": 1
                        },
                        "sprint_goal": "Por defecto",
                        "status": "EN_PLANIFICACION",
                        "iteration": 1,
                        "startDay": null,
                        "endDay": null
                    },
                    "name": "primer user story",
                    "description": "",
                    "priority": 3,
                    "estimate": 3,
                    "worked_hours": null,
                    "status": "NEW",
                    "member": 1
                },
                "worked_hours": 3.0,
                "description": "descripcion de una actividad editada",
                "created": "2021-10-28T02:05:20.317618Z"
            }
    """
    def put(self, req, id_project, id_us, id_activity):
        activity = get_object_or_404(Activity, pk=id_activity)
        user_story = get_object_or_404(UserStory, pk=id_us)

        last_worked_hours = activity.worked_hours

        serializer = UpdateActivitySerializer(data=req.data)
        if serializer.is_valid():
            updated_activity = serializer.update(activity, serializer.validated_data)
            updated_serializer = ListOrRetrieveActivitySerializer(updated_activity)

            # Se registra el evento de modificar comentario de user story
            EventoUserStory.objects.create(
                user_story=user_story, 
                user=req.user, 
                description="El usuario {0} modificó la actividad"
                            .format(req.user.email)
            )

            user_story.worked_hours = user_story.worked_hours - last_worked_hours

            user_story.worked_hours = user_story.worked_hours + updated_activity.worked_hours

            user_story.save()

            return Response(data=updated_serializer.data, status=status.HTTP_200_OK)
        
        return Response(data=serializer.errors,status=status.HTTP_400_BAD_REQUEST)

 

    def patch(self, req, id_project, id_us, id_activity):
            activity = get_object_or_404(Activity, pk=id_activity)

            last_worked_hours = activity.worked_hours

            user_story = get_object_or_404(UserStory, pk=id_us)

            serializer = UpdateActivitySerializer(data=req.data, partial=True)
            if serializer.is_valid():
                updated_activity = serializer.update(activity, serializer.validated_data)
                updated_serializer = ListOrRetrieveActivitySerializer(updated_activity)

                # Se registra el evento de modificar comentario de user story
                EventoUserStory.objects.create(
                    user_story=user_story, 
                    user=req.user, 
                    description="El usuario {0} modificó la actividad"
                                .format(req.user.email)
                )
                
                

                user_story.worked_hours = user_story.worked_hours - last_worked_hours

                user_story.worked_hours = user_story.worked_hours + updated_activity.worked_hours

                user_story.save()
                
                return Response(data=updated_serializer.data, status=status.HTTP_200_OK)
            
            return Response(data=serializer.errors,status=status.HTTP_400_BAD_REQUEST)

@api_view(["GET"])
def get_activities_by_sprint(req, id_project, id_sprint):
    activities = Activity.objects.filter(sprint=id_sprint)
    serializer = ListOrRetrieveActivitySerializer(activities, many=True)
    return Response(data=serializer.data, status=status.HTTP_200_OK)