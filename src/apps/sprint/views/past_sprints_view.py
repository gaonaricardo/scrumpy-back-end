from rest_framework import viewsets, status, views, permissions, mixins
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from sprint.models.sprint import Sprint
from sprint.serializers.past_sprints_serializer import ListOrRetrievePastSprintsSerializer, PastSprintsSerializer
from sprint.models.past_sprints import PastSprints

class PastSprintsView(views.APIView):
    def get(self, req, id_project, id_sprint):
        past_sprints_data = PastSprints.objects.filter(sprint=id_sprint)

        serializer = ListOrRetrievePastSprintsSerializer(past_sprints_data, many=True)

        return Response(data=serializer.data, status=status.HTTP_200_OK)