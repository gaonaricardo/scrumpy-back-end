from rest_framework.response import Response
from rest_framework import views
from sprint.serializers.evento_sprint_serializers import EventoSprintSerializer
from sprint.models.evento_sprint import EventoSprint
from sprint.serializers import EventoUserStorySerializer


class EventoSprintApiView(views.APIView):
    """
    * @api {get} /sprintModule/projects/:id_project/user_story/:id_sprint/history Listar Historial de Eventos de Sprints
    * @apiDescription Lista todos los eventos de los sprints de un proyecto.
    * @apiGroup Historial
    * @apiParam {Number} id_project El id del proyecto cuyo historial user stories se desean listar.
    * @apiParam {Number} id_sprint El id del User Story cuyo historial user stories se desean listar.
    * @apiSuccess {Object[]} evento_sprint Lista de eventos de sprints.
    * @apiSuccessExample {json} Success-Response:
        [
            {
                sprint: {
                    id: 1,
                    name: "Sprint 1",
                    description: "Sprint 1",
                    start_date: "2018-01-01",
                    end_date: "2018-01-31",
                    project: {
                        id: 1,
                        name: "Proyecto 1",
                        description: "Proyecto 1",
                        start_date: "2018-01-01",
                        end_date: "2018-01-31",
                        status: "ACTIVE",
                        member: {
                            id: 1,
                            name: "Miguel",
                            last_name: "Gonzalez",
                            email: ""
                        }
                    }
                },

                description: "Esto no es una descripción"
                user: {
                    id: 1,
                    name: "Miguel",
                    last_name: "Gonzalez",
                    email: ""
                },
                date: "2018-01-01",
            },
            {
                sprint: {
                    id: 1,
                    name: "Sprint 1",
                    description: "Sprint 1",
                    start_date: "2018-01-01",
                    end_date: "2018-01-31",
                    project: {
                        id: 1,
                        name: "Proyecto 1",
                        description: "Proyecto 1",
                        start_date: "2018-01-01",
                        end_date: "2018-01-31",
                        status: "ACTIVE",
                        member: {
                            id: 1,
                            name: "Miguel",
                            last_name: "Gonzalez",
                            email: ""
                        }
                    }
                },

                description: "Esto no es una descripción"
                user: {
                    id: 1,
                    name: "Miguel",
                    last_name: "Gonzalez",
                    email: ""
                },
                date: "2018-01-01",
            },
            {
                sprint: {
                    id: 1,
                    name: "Sprint 1",
                    description: "Sprint 1",
                    start_date: "2018-01-01",
                    end_date: "2018-01-31",
                    project: {
                        id: 1,
                        name: "Proyecto 1",
                        description: "Proyecto 1",
                        start_date: "2018-01-01",
                        end_date: "2018-01-31",
                        status: "ACTIVE",
                        member: {
                            id: 1,
                            name: "Miguel",
                            last_name: "Gonzalez",
                            email: ""
                        }
                    }
                },

                description: "Esto no es una descripción"
                user: {
                    id: 1,
                    name: "Miguel",
                    last_name: "Gonzalez",
                    email: ""
                },
                date: "2018-01-01",
            },
        ]
    """
    def get(self, request, id_project, id_sprint):
        evento_sprint = EventoSprint.objects.filter(sprint=id_sprint)
        eventJson = EventoSprintSerializer(evento_sprint, many=True)
        return self.response(eventJson.data)

    def response(self, data, status=200):
        return Response(data, status=status)