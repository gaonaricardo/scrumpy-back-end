from rest_framework import serializers
from sprint.serializers.user_story_serializers import ListOrRetrieveUserStorySerializer

from sprint.models.past_sprints import PastSprints

class PastSprintsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PastSprints
        fields = "__all__"

class ListOrRetrievePastSprintsSerializer(serializers.ModelSerializer):
    user_story = ListOrRetrieveUserStorySerializer(read_only=True)
    class Meta:
        model = PastSprints
        fields = "__all__"