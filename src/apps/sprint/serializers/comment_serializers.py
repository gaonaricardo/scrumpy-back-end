from rest_framework import serializers
from sprint.models.comentario import Comment
from proyecto.models import Member
from proyecto.serializers import ListOrRetrieveMemberSerializer
from sprint.models.user_story import UserStory
from sprint.serializers import ListOrRetrieveUserStorySerializer


class CreateCommentSerializer(serializers.ModelSerializer):
    member = serializers.PrimaryKeyRelatedField(
        queryset=Member.objects.all()
    )
    class Meta:
        model=Comment
        fields=['member', 'content']

class ListOrRetrieveCommentSerializer(serializers.ModelSerializer):
    member = ListOrRetrieveMemberSerializer(read_only=True)
    user_story = ListOrRetrieveUserStorySerializer(read_only=True)
    class Meta:
        model=Comment
        fields="__all__"

class UpdateCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model=Comment
        fields=["content"]