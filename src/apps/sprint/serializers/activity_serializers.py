from rest_framework import serializers
from sprint.models import user_story
from sprint.serializers.sprint_serializers import ListOrRetrieveSprintSerializer
from sprint.models.activity import Activity
from sprint.serializers import ListOrRetrieveUserStorySerializer
from autenticacion.serializers import CustomUserSerializer


class CreateActivitySerializer(serializers.ModelSerializer):
    user_story = serializers.PrimaryKeyRelatedField(
        queryset=user_story.UserStory.objects.all(),
        required = False,
    )
    created = serializers.DateField(format="YYYY-MM-DD")
    class Meta:
        model=Activity
        fields="__all__"

class ListOrRetrieveActivitySerializer(serializers.ModelSerializer):
    sprint = ListOrRetrieveSprintSerializer(read_only=True)
    user_story = ListOrRetrieveUserStorySerializer(read_only=True)
    user = CustomUserSerializer(read_only = True)
    class Meta:
        model=Activity
        fields="__all__"

class UpdateActivitySerializer(serializers.ModelSerializer):
    created = serializers.DateField(format="YYYY-MM-DD")
    class Meta:
        model=Activity
        fields=["worked_hours", "description", "created"]