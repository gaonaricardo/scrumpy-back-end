from rest_framework import serializers
from sprint.models.sprint import Sprint
from proyecto.models import Project
from proyecto.serializers import ListOrRetrieveProjectSerializer


class CreateSprintSerializer(serializers.ModelSerializer):
    """project = serializers.PrimaryKeyRelatedField(
        queryset=Project.objects.all()
    )"""
    class Meta:
        model=Sprint
        fields=['sprint_goal']

class ListOrRetrieveSprintSerializer(serializers.ModelSerializer):
    project = ListOrRetrieveProjectSerializer(read_only=True)
    class Meta:
        model=Sprint
        fields="__all__"

class UpdateSprintSerializer(serializers.ModelSerializer):
    class Meta:
        model=Sprint
        fields=("sprint_goal", "status", "startDay", "endDay","iteration")