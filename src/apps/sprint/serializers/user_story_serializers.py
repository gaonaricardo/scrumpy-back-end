from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from proyecto.models.member import Member
from rest_framework.views import APIView
from proyecto.models import Project
from sprint.models.sprint import Sprint
from sprint.models.user_story import UserStory
from proyecto.serializers import ListOrRetrieveProjectSerializer
from sprint.serializers.sprint_serializers import ListOrRetrieveSprintSerializer

class ReplaceMemberUserStorySerializer(APIView):
    def post(self,req,id_project):
        miembroReemplazado = Member.objects.get(id=req.data['id_member_reemplazado']).id
        
        miembroReemplazo = Member.objects.get(id=req.data['id_member_reemplazo']).id
        
        project = id_project
        
        user_story_member_reemplazado = UserStory.objects.filter(member=miembroReemplazado,project=project)
        
        
        for us in user_story_member_reemplazado:    
            us.update(member=miembroReemplazo)
            
        return Response(status=status.HTTP_200_OK,data=ListOrRetrieveUserStorySerializer(user_story_member_reemplazado,many=True).data)
        
    pass


class CreateUserStorySerializer(serializers.ModelSerializer):
    class Meta:
        model=UserStory
        fields=['name', 'description', 'priority']

class ListOrRetrieveUserStorySerializer(serializers.ModelSerializer):
    project = ListOrRetrieveProjectSerializer(read_only=True)
    sprint = ListOrRetrieveSprintSerializer(read_only=True)
    class Meta:
        model=UserStory
        fields="__all__"

class UpdateUserStorySerializer(serializers.ModelSerializer):
    project = serializers.PrimaryKeyRelatedField(
        queryset=Project.objects.all()
    )
    sprint = serializers.PrimaryKeyRelatedField(
        queryset=Sprint.objects.all(), allow_null=True
    )
    member = serializers.PrimaryKeyRelatedField(
        queryset=Member.objects.all(), allow_null=True
    )
    class Meta:
        model=UserStory
        fields="__all__"