from rest_framework import serializers
from sprint.models import EventoSprint

class EventoSprintSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventoSprint
        fields = "__all__"