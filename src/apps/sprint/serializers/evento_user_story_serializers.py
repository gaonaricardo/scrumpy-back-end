from rest_framework import serializers
from sprint.models.evento_user_story import EventoUserStory
from autenticacion.serializers import CustomUserSerializer

class EventoUserStorySerializer(serializers.ModelSerializer):
    class Meta:
        model = EventoUserStory
        fields = "__all__"

class ListOrRetrieveEventoUserStorySerializer(serializers.ModelSerializer):
    user = CustomUserSerializer(read_only = True)
    class Meta:
        model=EventoUserStory
        fields="__all__"