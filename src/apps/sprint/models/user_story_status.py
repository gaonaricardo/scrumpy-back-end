from django.db import models

class UserStoryStatus(models.TextChoices):
    NEW = 'NEW'
    TODO = 'TODO'
    DOING = 'DOING'
    DONE = 'DONE'
    UNFINISHED = 'UNFINISHED'
    REJECTED = 'REJECTED'
    ARCHIVED = 'ARCHIVED'
    TESTING = 'TESTING'
    RELEASE = 'RELEASE'