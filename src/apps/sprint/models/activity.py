from django.db import models
from sprint.models import Sprint
from sprint.models.user_story import UserStory
from autenticacion.models import CustomUser

class Activity(models.Model):
    user = models.ForeignKey(CustomUser, null=True, blank=True, on_delete=models.CASCADE)
    user_story = models.ForeignKey(UserStory,null=True, blank=True, on_delete=models.CASCADE)
    sprint = models.ForeignKey(Sprint, null=True, blank=True, on_delete=models.SET_NULL)
    worked_hours = models.FloatField(
        null=True,
        blank=True,
    )
    description = models.TextField(null=True, blank=True)
    created = models.DateField(null=True, blank=True)
    