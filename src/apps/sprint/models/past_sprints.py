from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from proyecto.models.member import Member
from sprint.models.sprint import Sprint
from sprint.models.user_story import UserStory

class PastSprints(models.Model):
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE)
    user_story = models.ForeignKey(UserStory, on_delete=models.PROTECT)
    story_status = models.CharField(max_length=10)
    priority = models.IntegerField(
        validators=[
            MaxValueValidator(10), 
            MinValueValidator(1),
        ],
        null=True,
        blank=True,
        default=1,
    )
    member = models.ForeignKey(Member, on_delete=models.SET_NULL, null=True)
    estimate = models.IntegerField(
        validators=[
            MaxValueValidator(10), 
            MinValueValidator(1),
        ],
        null=True,
        blank=True,
    )
    worked_hours = models.FloatField(
        null=True,
        blank=True,
        default=0,
    )
    