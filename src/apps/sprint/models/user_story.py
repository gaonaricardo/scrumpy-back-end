from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from proyecto.models import Project, Member
from sprint.models import UserStoryStatus
from sprint.models import Sprint

class UserStory(models.Model):
    project = models.ForeignKey(Project, null=True, on_delete=models.CASCADE)
    sprint = models.ForeignKey(Sprint, null=True, on_delete=models.SET_NULL)
    member = models.ForeignKey(Member, on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    priority = models.IntegerField(
        validators=[
            MaxValueValidator(10), 
            MinValueValidator(1),
        ],
        null=True,
        blank=True,
        default=1,
    )
    estimate = models.FloatField(
        validators=[ 
            MinValueValidator(1),
        ],
        null=True,
        blank=True,
    )
    worked_hours = models.FloatField(
        null=True,
        blank=True,
        default=0,
    )
    status = models.CharField(
        max_length=10, 
        choices=UserStoryStatus.choices, 
        default=UserStoryStatus.NEW,
    )
    finish_date = models.DateField(null=True, blank=True)

    