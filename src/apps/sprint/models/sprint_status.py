from django.db import models

"""
    Modelo SprintStatus

    models.TextChoices: {
        EN_PLANIFICACION
        INICIADO
        FINALIZADO
    }
    
"""

class SprintStatus(models.TextChoices):
    EN_PLANIFICACION = 'EN_PLANIFICACION'
    INICIADO = 'INICIADO'
    FINALIZADO = 'FINALIZADO'