from django.db import models

from sprint.models import UserStory
from autenticacion.models import CustomUser
from django.utils import timezone

class EventoUserStory(models.Model):
    user_story = models.ForeignKey(UserStory, on_delete=models.CASCADE)
    description = models.TextField(default="")
    user = models.ForeignKey(CustomUser, on_delete=models.PROTECT, null=True)
    date = models.DateField(default=timezone.now)
    
    def __str__(self):
        return self.description