# Django
from django.test import TestCase

# Python
import json

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status

# Models
from autenticacion.models import CustomUser
from proyecto.models import Project
from sprint.models import Sprint


class SprintTestCase(TestCase):

    def test_create_sprint(self):

        client = APIClient()

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master= user,
        )

        test_sprint = {
            'sprint_goal': 'Objetivo del Sprint',
            'project': proyecto.id,
        }

        response = client.post(
            f'/api/sprintModule/projects/{proyecto.id}/sprints/',
            test_sprint,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', result)
        self.assertIn('sprint_goal', result)
        self.assertIn('project', result)

    def test_update_sprint(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master= user,
        )

        sprint = Sprint.objects.create(
            sprint_goal='Objetivo del sprint',
            project=proyecto,
        )

        test_sprint_update = {
            'sprint_goal': 'Objetivo del Sprint_update',
            'project': proyecto.id,
        }

        response = client.put(
            f'/api/sprintModule/projects/{proyecto.id}/sprints/{sprint.id}/',
            test_sprint_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_sprint(self):

        client = APIClient()

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master= user,
        )

        sprint1 = Sprint.objects.create(
            sprint_goal='Sprint_goal_test1',
            project=proyecto
        )

        sprint2 = Sprint.objects.create(
            sprint_goal='Sprint_goal_test2',
            project=proyecto
        )
 
        response = client.get(
            f'/api/sprintModule/projects/{proyecto.id}/sprints/')

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        for sprint in result:
            self.assertIn('id', sprint)
            self.assertIn('sprint_goal', sprint)
            self.assertIn('project', sprint)
            break

    def test_create_sprint_unauthorized(self):

        client = APIClient()

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master= user,
        )

        test_sprint = {
            'sprint_goal': 'Objetivo del Sprint',
            'project': proyecto.id,
        }

        response = client.post(
            f'/api/sprintModule/projects/{proyecto.id}/sprints/',
            test_sprint,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_sprint_unauthorized(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master= user,
        )

        sprint = Sprint.objects.create(
            sprint_goal='Objetivo del sprint',
            project=proyecto,
        )

        test_sprint_update = {
            'sprint_goal': 'Objetivo del Sprint_update',
            'project': proyecto.id,
        }

        response = client.put(
            f'/api/sprintModule/projects/{proyecto.id}/sprints/{sprint.id}/',
            test_sprint_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_sprint_unauthorized(self):

        client = APIClient()

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master= user,
        )

        sprint1 = Sprint.objects.create(
            sprint_goal='Sprint_goal_test1',
            project=proyecto
        )

        sprint2 = Sprint.objects.create(
            sprint_goal='Sprint_goal_test2',
            project=proyecto
        )
 
        response = client.get(
            f'/api/sprintModule/projects/{proyecto.id}/sprints/')

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
