# Django
from django.test import TestCase

# Python
import json

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status

# Models
from autenticacion.models import CustomUser
from proyecto.models import Project, Member
from sprint.models import UserStory, Activity, Sprint

from rest_framework_jwt.utils import jwt_payload_handler, jwt_encode_handler


class ActivityTestCase(TestCase):

    def get_token(self, user):
        payload = jwt_payload_handler(user)
        encode = jwt_encode_handler(payload=payload)
        return encode

    def test_create_activity(self):

        client = APIClient()

        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=usuario)

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=usuario
        )

        sprintx = Sprint.objects.create(
            sprint_goal='Objetivo del sprint',
            project=proyecto,
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
            sprint=sprintx,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        test_activity = {
            'worked_hours' : 1,
            'description': 'Descripcion de la actividad',
        }

        response = client.post(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/activities/',
            test_activity,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', result)
        self.assertIn('description', result)
        self.assertIn('worked_hours', result)

    def test_update_activity(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=usuario)

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=usuario
        )

        sprintx = Sprint.objects.create(
            sprint_goal='Objetivo del sprint',
            project=proyecto,
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
            sprint=sprintx,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        activity = Activity.objects.create(
            sprint = sprintx,
            user_story = user_s,
            description = 'Contenido',
            worked_hours = 1,
        )
        """sprint.Project.add(proyecto)"""

        test_activity_update = {
            'worked_hours' : 1,
            'description': 'Descripcion de la actividad',
        }

        response = client.put(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/activities/{activity.id}/',
            test_activity_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_get_activity(self):

        client = APIClient()

        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )
        
        client.force_authenticate(user=usuario)

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=usuario
        )

        sprintx = Sprint.objects.create(
            sprint_goal='Objetivo del sprint',
            project=proyecto,
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
            sprint=sprintx,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        activity1 = Activity.objects.create(
            sprint = sprintx,
            user_story = user_s,
            description = 'Contenido',
            worked_hours = 1,
        )

        activity2 = Activity.objects.create(
            sprint = sprintx,
            user_story = user_s,
            description = 'Contenido',
            worked_hours = 1,
        )
 
        response = client.get(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/activities/')

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        for sprint in result:
            self.assertIn('id', sprint)
            self.assertIn('description', sprint)
            self.assertIn('worked_hours', sprint)
            break

    def test_create_activity_unauthorized(self):

        client = APIClient()

        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=usuario
        )

        sprintx = Sprint.objects.create(
            sprint_goal='Objetivo del sprint',
            project=proyecto,
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
            sprint=sprintx,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        test_activity = {
            'worked_hours' : 1,
            'description': 'Descripcion de la actividad',
        }

        response = client.post(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/activities/',
            test_activity,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_activity_unauthorized(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=usuario
        )

        sprintx = Sprint.objects.create(
            sprint_goal='Objetivo del sprint',
            project=proyecto,
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
            sprint=sprintx,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        activity = Activity.objects.create(
            sprint = sprintx,
            user_story = user_s,
            description = 'Contenido',
            worked_hours = 1,
        )
        """sprint.Project.add(proyecto)"""

        test_activity_update = {
            'worked_hours' : 1,
            'description': 'Descripcion de la actividad',
        }

        response = client.put(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/activities/{activity.id}/',
            test_activity_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


    def test_get_activity_unauthorized(self):

        client = APIClient()

        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )
    

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=usuario
        )

        sprintx = Sprint.objects.create(
            sprint_goal='Objetivo del sprint',
            project=proyecto,
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
            sprint=sprintx,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        activity1 = Activity.objects.create(
            sprint = sprintx,
            user_story = user_s,
            description = 'Contenido',
            worked_hours = 1,
        )

        activity2 = Activity.objects.create(
            sprint = sprintx,
            user_story = user_s,
            description = 'Contenido',
            worked_hours = 1,
        )
 
        response = client.get(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/activities/')

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)