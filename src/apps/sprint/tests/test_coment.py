# Django
from django.test import TestCase

# Python
import json

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status

# Models
from autenticacion.models import CustomUser
from proyecto.models import Project, Member
from sprint.models import UserStory, Comment

from rest_framework_jwt.utils import jwt_payload_handler, jwt_encode_handler


class CommentTestCase(TestCase):

    def get_token(self, user):
        payload = jwt_payload_handler(user)
        encode = jwt_encode_handler(payload=payload)
        return encode

    def test_create_comment(self):

        client = APIClient()

        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=usuario)

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=usuario
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        test_comment = {
            'member' : miembro.id,
            'content': 'Contenido del comentario',
        }

        response = client.post(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/comments/',
            test_comment,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', result)
        self.assertIn('member', result)
        self.assertIn('content', result)

    def test_update_comment(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=usuario)

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=usuario
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        comment = Comment.objects.create(
            member = miembro,
            user_story = user_s,
            content = 'Contenido'
        )
        """sprint.Project.add(proyecto)"""

        test_comment_update = {
            'member' : miembro.id,
            'content': 'Contenido del comentario',
        }

        response = client.put(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/comments/{comment.id}/',
            test_comment_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_comment(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )
        client.force_authenticate(user=usuario)
        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = usuario 
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )
        
        comment = Comment.objects.create(
            member = miembro,
            content = 'Contenido',
            user_story = user_s
        )

        response = client.delete(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/comments/{comment.id}/', 
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        comment_exists = Comment.objects.filter(id=comment.id)
        self.assertFalse(comment_exists)

    def test_get_comment(self):

        client = APIClient()

        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )
        
        client.force_authenticate(user=usuario)

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=usuario
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        comment1 = Comment.objects.create(
            member = miembro,
            user_story = user_s,
            content = 'Contenido'
        )

        comment2 = Comment.objects.create(
            member = miembro,
            user_story = user_s,
            content = 'Contenido'
        )
 
        response = client.get(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/comments/')

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        for sprint in result:
            self.assertIn('id', sprint)
            self.assertIn('member', sprint)
            self.assertIn('content', sprint)
            break

    def test_create_comment_unauthorized(self):

        client = APIClient()

        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )


        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=usuario
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        test_comment = {
            'member' : miembro.id,
            'content': 'Contenido del comentario',
        }

        response = client.post(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/comments/',
            test_comment,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_comment_unauthorized(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )


        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=usuario
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        comment = Comment.objects.create(
            member = miembro,
            user_story = user_s,
            content = 'Contenido'
        )
        """sprint.Project.add(proyecto)"""

        test_comment_update = {
            'member' : miembro.id,
            'content': 'Contenido del comentario',
        }

        response = client.put(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/comments/{comment.id}/',
            test_comment_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_comment_unauthorized(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )
        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = usuario 
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )
        
        comment = Comment.objects.create(
            member = miembro,
            content = 'Contenido',
            user_story = user_s
        )

        response = client.delete(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/comments/{comment.id}/', 
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_comment_unauthorized(self):

        client = APIClient()

        usuario = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )
        

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=usuario
        )

        user_s = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
        )

        miembro = Member.objects.create(
            user= usuario,
            project= proyecto,
        )

        comment1 = Comment.objects.create(
            member = miembro,
            user_story = user_s,
            content = 'Contenido'
        )

        comment2 = Comment.objects.create(
            member = miembro,
            user_story = user_s,
            content = 'Contenido'
        )
 
        response = client.get(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_s.id}/comments/')

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

