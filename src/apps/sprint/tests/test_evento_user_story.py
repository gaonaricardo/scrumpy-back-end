# Django
from django.test import TestCase

# Python
import json

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status

# Models
from autenticacion.models import CustomUser
from proyecto.models import Project
from sprint.models import Sprint

class EventoUserStoryTestCase(TestCase):

    def test_get_evento_user_story(self):

        client = APIClient()

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=user
        )


        test_user_story = {
            'name': 'Nombre del US',
            'description': 'Descripcion del US',
            'priority': 1,
            'project': proyecto.id,
        }

        response = client.post(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/',
            test_user_story,
            format='json'
        )

        result = json.loads(response.content)
 
        response = client.get(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{0}/history/')

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_evento_user_story_unauthorized(self):

        client = APIClient()

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=user
        )


        test_user_story = {
            'name': 'Nombre del US',
            'description': 'Descripcion del US',
            'priority': 1,
            'project': proyecto.id,
        }

        response = client.post(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/',
            test_user_story,
            format='json'
        )

        result = json.loads(response.content)
 
        response = client.get(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{0}/history/')

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)