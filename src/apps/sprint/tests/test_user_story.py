# Django
from django.test import TestCase

# Python
import json

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status
from sprint.models.user_story import UserStory

# Models
from autenticacion.models import CustomUser
from proyecto.models import Project


class UserStoryTestCase(TestCase):

    def test_create_user_story(self):

        client = APIClient()

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=user,
        )

        test_user_story = {
            'name': 'Nombre del US',
            'description': 'Descripcion del US',
            'priority': 1,
            'project': proyecto.id,
        }

        response = client.post(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/',
            test_user_story,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', result)
        self.assertIn('name', result)
        self.assertIn('description', result)
        self.assertIn('project', result)

    def test_update_user_story(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=user,
        )

        user_story = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
        )

        test_user_story_update = {
            'name': 'Nombre del US_update',
            'description': 'Descripcion del US',
            'priority': 1,
            'project': proyecto.id,
        }

        response = client.patch(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_story.id}/',
            test_user_story_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_user_story(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = user  
        )
        
        user_story = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project = proyecto,
        )


        response = client.delete(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_story.id}/', 
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_user_story(self):

        client = APIClient()

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        client.force_authenticate(user=user)

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=user,
        )

        user_story1 =UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto
        )

        user_story2 =UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto
        )
 
        response = client.get(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/')

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        for userStory in result:
            self.assertIn('id', userStory)
            self.assertIn('name', userStory)
            self.assertIn('description', userStory)
            self.assertIn('project', userStory)
            break

    def test_create_user_story_unauthorized(self):

        client = APIClient()

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=user,
        )

        test_user_story = {
            'name': 'Nombre del US',
            'description': 'Descripcion del US',
            'priority': 1,
            'project': proyecto.id,
        }

        response = client.post(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/',
            test_user_story,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        
    def test_update_user_story_unauthorized(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=user,
        )

        user_story = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto,
        )

        test_user_story_update = {
            'name': 'Nombre del US_update',
            'description': 'Descripcion del US',
            'priority': 1,
            'project': proyecto.id,
        }

        response = client.patch(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_story.id}/',
            test_user_story_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_user_story_unauthorized(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
                name = 'proyecto1_test',
                description = 'Este es el proyecto 1',
                scrum_master = user  
        )
        
        user_story = UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project = proyecto,
        )


        response = client.delete(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/{user_story.id}/', 
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_user_story_unauthorized(self):

        client = APIClient()

        user = CustomUser.objects.create(
            email='ramako72@gmail.com',
            first_name='Raul',
            last_name='Galvan'
        )

        proyecto = Project.objects.create(
            name='proyecto1_test',
            description='Este es el proyecto 1',
            scrum_master=user,
        )

        user_story1 =UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto
        )

        user_story2 =UserStory.objects.create(
            name = 'Nombre del US',
            description = 'Descripcion',
            priority = 1,
            project=proyecto
        )
 
        response = client.get(
            f'/api/sprintModule/projects/{proyecto.id}/user_stories/')

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)