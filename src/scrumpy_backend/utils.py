from django.core.mail import send_mail
from django.conf import settings

def enviar_email(subject, content, email_to):

    mail_from = settings.EMAIL_HOST_USER
    mail_to = list()
    mail_to.append(email_to)

    send_mail(subject, content, mail_from, mail_to, fail_silently=False)
