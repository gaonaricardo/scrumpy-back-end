# Script
# 1. Clonar el Repositorio ✅
# 2. Seleccionar Tag de Iteracion ✅
# 3. Seleccionar el entorno (Desarrollo, Produccion) ✅
# 3. Desplegar ✅
# 4. Poblar la BD con Datos de Prueba ✅

#!/bin/bash
function clonarProyecto() {
    echo "Clonando el proyecto..."
    git clone https://gitlab.com/ramirez-sebas1010/scrumpy-front-end.git
    echo "Clonado completado"
}

if [ $# -ne 2 ]; then
    echo "Usage: $0 <tag> <entorno>"
    echo "Example: $0 iteracion6 produccion"
    echo "Example: $0 iteracion6 desarrollo"
    exit 1
fi

clonarProyecto
echo "Descargando el pm2"

npm install pm2@latest -g
pm2 list
pm2 delete 0

cd scrumpy-front-end
echo "Descargando las Dependencias del proyecto"

echo "Los Tags son:"

git tag -l | grep -w $1

echo "Iremos al tag '$1'"
if [ $1 == "iteracion_3" ]
    then
    git checkout iteracion3
else
    git checkout $1
fi

npm i --save



if [  $2 = "desarrollo" ]; then
    echo "Entorno: Desarrollo"
    echo "Iniciando"
    pm2 start --name scrumpy-front-end npm -- start

    [  $? -ne 0 ] && echo "Error al iniciar el proyecto" && exit 1
    exit 0
else
    echo "Entorno: Produccion"
    pm2 start --name scrumpy-front-end npm -- run prod -- --port 3001
fi