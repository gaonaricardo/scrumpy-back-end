docker-compose down
sudo rm -rf database
mkdir database
docker-compose up -d --build
docker exec -it scrumpy-back-end_web_1 python src/manage.py createsuperuser
docker exec -it scrumpy-back-end_db_1 psql -U postgres -d postgres -f home/load_data.sql
