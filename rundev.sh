source venv/bin/activate

python3 src/manage.py makemigrations

python3 src/manage.py migrate

python3 src/manage.py collectstatic --no-input
#FIX: Genera la documentacion en el src 
bash generate_doc.sh

python3 src/manage.py runserver 0.0.0.0:8000
