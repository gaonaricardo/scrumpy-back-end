#!/bin/bash

# Script para Backend
# 1. Clonar el Repositorio ✅
# 2. Seleccionar Tag de Iteracion ✅
# 3. Seleccionar el entorno (Desarrollo, Produccion) ✅
# 4. Desplegar en el entorno de Desarrollo ✅ 
# 6. Poblar la BD en el Entorno de Desarrollo con Datos de Prueba ✅
# 5. Desplegar en el entorno de Produccion ✅
# 6. Poblar la BD en el Entorno de Produccion con Datos de Prueba. ✅

function clonarProyecto() {
    docker stop $(docker ps -a -q)
    docker rm $(docker ps -a -q)
    git clone
    echo "Clonando el proyecto..."
    git clone https://gitlab.com/gaonaricardo/scrumpy-back-end.git
}

if [ $# -ne 2 ]; then
    echo "Usage: $0 <tag | rama> <entorno>"
    echo "Example: $0 iteracion1 produccion"
    echo "Example: $0 iteracion_3 desarrollo"
    exit 1
fi

clonarProyecto

cd scrumpy-back-end

echo "Los Tags son:"
git tag -l | grep -w $1
        
echo "Iremos al tag '$1'"

git checkout $1

[ $1 == "iteracion1" ] || [ $1 == "iteracion2" ] && cd ScrumpyBackend
touch .env

echo 'DB_NAME_DEV=postgres' >> .env
echo 'DB_USER_DEV=postgres' >> .env
echo 'DB_PASSWORD_DEV=postgres' >> .env
if [ $1 == "iteracion2" ]; then
    echo 'DB_HOST_DEV=localhost' >> .env
else
    echo 'DB_HOST_DEV=db' >> .env
fi
echo 'DB_PORT_DEV=5432' >> .env

echo 'DB_NAME_PROD=postgres' >> .env
echo 'DB_USER_PROD=postgres' >> .env
echo 'DB_PASSWORD_PROD=postgres' >> .env
if [ $1 == "iteracion2" ]; then
    echo 'DB_HOST_PROD=localhost' >> .env
else
    echo 'DB_HOST_PROD=db' >> .env
fi
echo 'DB_PORT_PROD=5432' >> .env

echo 'SECRET_KEY=)7=5$^w=rq-ysjm4!7((g+(pii)a7m4)jast8se&r#$&c+$dn7' >> .env
echo 'CLIENT_ID=318530030530-hvv8pllgdbskr04clsacnd6qq4uchjng.apps.googleusercontent.com' >> .env

[ $1 == "iteracion1" ] && cd ..

if [  $2 = "desarrollo" ]; then
    
    echo "Entorno: Desarrollo"

    if [ $1 != "iteracion2" ]; then
        docker-compose -f docker-compose.yml up -d --build
    else
        docker stop $(docker ps -a -q)
        docker run --rm -it -d --name db -p 5432:5432 -e POSTGRES_PASSWORD=postgres -e POSTGRES_USER=postgres -e POSTGRES_DB=postgres postgres
    fi 
    echo
    echo "Poblando la BD con datos de prueba"
    sleep 10
    COUNT=0
    while ! curl --location --request POST 'http://localhost:8000/api/projectModule/poblarBD/'
    do  
        { echo "Exit status of curl: $?"
          echo "Retrying ..."
        } 1>&2
        sleep 15
        COUNT=$((COUNT+1))
        if [ $COUNT -eq 3 ]; then
            echo "Se ha excedido el numero de intentos"
            exit 0
        fi
    done
    echo "Desplegado en Desarrollo"       
    
    exit 0
else
    echo "Entorno: Produccion"
    
    if [ $1 != "iteracion2" ]; then
        docker-compose -f docker-compose-prod.yml up -d --build
    else
        docker stop $(docker ps -a -q)
        docker run --rm -it -d --name db -p 5432:5432 -e POSTGRES_PASSWORD=postgres -e POSTGRES_USER=postgres -e POSTGRES_DB=postgres postgres
    fi 
    sleep 10
    COUNT=0
    while ! curl --location --request POST 'http://localhost:8001/api/projectModule/poblarBD/'
    do  
        { echo "Exit status of curl: $?"
          echo "Retrying ..."
        } 1>&2
        sleep 5
        COUNT=$((COUNT+1))
        if [ $COUNT -eq 3 ]; then
            echo "Se ha excedido el numero de intentos"
            exit 0
        fi
    done

    echo "Desplegado en Produccion"
    exit 0
fi