# Poblar la base de datos con datos de prueba
- Levantar el backend con docker-compose up --build
- Crear el superuser con docker exec -it scrumpy-back-end_web_1 python src/manage.py createsuperuser
- Ejecutar el script con docker exec -it scrumpy-back-end_db_1 psql -U postgres -d postgres -f home/load_data.sql
