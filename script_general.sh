#!/bin/bash

#Script General para generar el Repositorio del Backend y Frontend

function assignPermissions() {
    sudo chmod 777 -R .
    echo "Permisos correctamente asignados"
}

function deleteRepositories() {
    echo "Eliminando repositorios"
    if [ -d "./scrumpy-front-end" ]; then
        rm -rf ./scrumpy-front-end
        echo "Se ha eliminado el repostorio scrumpy-front-end"
    fi

    if [ -d "./scrumpy-back-end" ]; then
        rm -rf scrumpy-back-end
        echo "Se ha eliminado el repostorio scrumpy-back-end"
    fi

    [ $? -ne 0 ] && echo "No se ha encontrado uno de los repositorios"
}

if [ $# -ne 2 ]; then
    echo "Usage: $0 <tag> <entorno>"
    echo "Example: $0 iteracion6 produccion"
    echo "Example: $0 iteracion6 desarrollo"
    exit 1
fi

#Verificamos que el entorno sea valido.
[ $2 != "desarrollo" ] && [ $2 != "produccion" ] && echo "El entorno debe ser desarrollo o produccion" && exit 1

#Asignamos los permisos necesarios.
assignPermissions

#Eliminamos los repositorios antiguas si existe.
deleteRepositories

#Script que levanta el Backend
echo "Creando el Backend..."
bash script_backend.sh $1 $2

[ $? -ne 0 ] && echo "Error al crear el Backend" && exit 1

echo "Creando el Frontend"
bash script_frontend.sh $1 $2

[ $? -ne 0 ] && echo "Error al crear el Frontend" exit 1

echo "Se ha creado exitosamente el proyecto"


# # curl --location --request POST 'http://localhost:8000/api/projectModule/poblarBD/'
    